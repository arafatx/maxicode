-- This is a lua script that is used by suricata to generate custom report and send email
-- Author: Arafat Ali | Email: arafat@sofibox.com

-- (DONE NEED TESTING) TODO 9 - Clear report cache by timestamp for blacklist_scan, aipdb cache and reported_ip. For example clear after 1 or 2 hours (every cache already has timestamp) (done all need testing)
-- TODO 10 - If getting from whitelist, to block whitelist from not showing, we also need to consider the score is bad or not, if the score is not bad, blacklist is 0, susplog file is 0, we suppress. So suppressing IP doesn't mean we suppress forever, it must have condition (important feature)
-- TODO 12 - Suggest at 'Your Action' if IP in a network more than 7 then we can put it in CIDR format blocking to reduce IP limit in CSF / increase performance (done need testing)
-- TODO 13 - Create IGNORE LIST (done need testing)
-- TODO 14 - Improve SUSPICIOUS LOG FILE detection with advanced regular expression for each log
-- TODO 15 - create remove function from suppress ip list, from suppress cache list, from whitelist, from blacklist..
-- TODO 16 - suppress message list need to have comment and timestamp
-- TODO 17 - Auto suggest to add into ignore list (for bad IP), if csf already blocked before
-- TODO 18 - remember IP in whitelist or suppress list can be bad, so we need to frequently detect it action. if score_status = bad then remove from whitelist.
-- TODO 19 - Improve message attack when reporting to AIPDB
-- TODO 20 - Use other method of RBL for ipv6.
-- TODO 21 - Make my own blocklist, Create a block list file from that github, use it to compare IP with this list.
-- TODO 22 - Enable CIDR16
-- TODO 23 - summarize what kind of attack based on suspicious log (suspicious log info can determine accurate attack). Example if get log has this "POST /xmlrpc.php" confirmed wordpress bruteforce. we make variable (suspicious_log_message)
-- TODO 24 - build captcha for blocked users from CSF

-- This is a popen wrapper function used to handle and destroy file handler for readline output command
-- Without this function code will be messy with handle:close() method when using popen function
function io.popen_readline(cmd)
    local handle = io.popen(cmd)
    local result = handle:read("*line")
    handle:close()
    return result
end

-- This is a popen wrapper function used to handle and destroy file handler for readall output command
-- Without this function code will be messy with handle:close() method when using popen function
function io.popen_readall(cmd)
    local handle = io.popen(cmd)
    local result = handle:read("*all")
    handle:close()
    return result
end

-- This is a popen wrapper function used to handle and destroy file handler (no output to return)
function io.popen_x(cmd)
    local handle = io.popen(cmd)
    handle:close()
end

-- This is an OS specific function useful to sleep suricata statement in this script. Similar to sleep(n) in bash
function sleep(n)
    os.execute("sleep " .. tonumber(n))
end

-- This function is used whether to display log function in suricata.log or not (For Debug)
-- To enable set enable_log = true
function logNotice(logstr)
    if enable_log == true then
        log_pattern = "Maxicata: " .. logstr
        SCLogNotice(log_pattern)
        if enable_self_log == true then
            maxicata_rpt:write(log_pattern .. "\n")
            maxicata_rpt:flush()
        end
    end
end

function csf_execute_block(rule)
    logNotice("=== START CSF Block Status ===")
    if enable_csf_auto_block == true then
        logNotice("CSF auto block is enabled. Using rule: " .. rule)
        -- Blocking this IP using maxicata -c deny <ip>
        cmd_auto_blockstr = "maxicata -c deny " .. susp_ip .. " 'Suricata_Blocked by " .. rule .. " | GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_code .. "| Abuse_Percentage:" .. aipdb_abuse_score
                .. "% | Alert_Level: " .. alert_level .. " |RBL_Blacklisted_Count: " .. blacklist_count .. "| Inbound_Outbound: " .. src_dst_count .. "'"
        csf_block_status = io.popen_readline(cmd_auto_blockstr)
        -- "true" means it's in permanent blocked list
        if csf_block_status == "true" then
            is_csf_blocked = "yes"
            csf_block_status = "CSF has already blocked this IP before"
            logNotice("CSF has already blocked this IP. Block Status is: " .. csf_block_status)
        end
        -- Give generic action
        alert_level_action = csf_block_status
    else
        logNotice("CSF auto block is disabled but this IP has bad rule")
        -- Give action when enable_csf_auto_block is not set
        alert_level_action = "CSF auto block is not set but the bad rule has been triggered for this IP. Block this IP immediately"
    end
    logNotice("=== END CSF Block Status ===")
end

-- This function is used to get information about an IP from abuseipdb.com using API. It has wrapper function from maxicata -c <command>
function set_aipdb_cached_data(ip)
    logNotice("=== START AIPDB cache IP info ===")
    logNotice("Checking existing IP " .. ip .. " for cache")
    -- First make sure that the IP we want to cache is not exist in the cache file. Use wrapper: maxicata -c is-aipdb-ip-cached <ip>
    is_aipdb_ip_cached = io.popen_readline("maxicata -c 'is-aipdb-ip-cached' '" .. ip .. "'")
    logNotice("IS IP cached?: " .. is_aipdb_ip_cached)
    if is_aipdb_ip_cached == "false" then
        -- Meaning if an IP does not exist in cache,
        logNotice("Notice, the IP " .. ip .. " is not cached in database file!")
        logNotice("Now caching IP " .. ip .. " info from AIPDB (this may take sometimes) ... ")
        aipdb_ip_cache_status = io.popen_readline("maxicata -c 'aipdb-cache-ipinfo' '" .. ip .. "'") -- Add new variable TODO
        -- Set it as a new IP label for the first time
        aipdb_cached = "new"
    else
        -- "yes"
        logNotice("OK, " .. ip .. " is cached in database file!")
        -- Set it as an old IP label because the cached is inside the database
        aipdb_ip_cache_status = "AIPDB API was not called. Using cached IP info"
        aipdb_cached = "cached"
    end
    logNotice("Cache status: " .. aipdb_ip_cache_status)
    -- Then after the above condition, we read the IP cache information and assign each into variable.
    -- Usage: maxicata -c aipdb-get-cached-var <ip> <variable_field>
    -- Example: maxicata -c aipdb-get-cached-var 1.1.1.153 abuse_score, will return abuse_score value
    logNotice("Assigning all variables from cached file ...")
    aipdb_ip = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'ip'")
    aipdb_iswhitelisted = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'iswhitelisted'")
    aipdb_abuse_score = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'abuse_score'")
    aipdb_isp = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " isp")
    aipdb_usage_type = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'usage_type'")
    aipdb_domain = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'domain'")
    aipdb_country_name = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'country_name'")
    aipdb_country_code = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'country_code'")
    aipdb_total_report = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'total_report'")
    aipdb_distinct_report = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'distinct_report'")
    aipdb_last_report = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'last_report'")
    aipdb_time = io.popen_readline("maxicata -c aipdb-get-cached-var " .. ip .. " 'time'")

    -- Debug variable in log (incase concatenation problem, for example string cannot concatenate with nil or number, then separate the variable from string)
    logNotice("This is a(n) " .. aipdb_cached .. " ip information:")
    logNotice("---------------------START------------------------")
    logNotice("aipdb_ip: " .. aipdb_ip .. "| type: " .. type(aipdb_ip))
    logNotice("aipdb_iswhitelisted: " .. aipdb_iswhitelisted .. "| type: " .. type(aipdb_iswhitelisted))
    logNotice("aipdb_abuse_score: " .. aipdb_abuse_score .. "| type: " .. type(aipdb_abuse_score))
    logNotice("aipdb_isp: " .. aipdb_isp .. "| type: " .. type(aipdb_isp))
    logNotice("aipdb_usage_type: " .. aipdb_usage_type .. "| type: " .. type(aipdb_usage_type))
    logNotice("aipdb_domain: " .. aipdb_domain .. "| type: " .. type(aipdb_domain))
    logNotice("aipdb_country_name: " .. aipdb_country_name .. "| type: " .. type(aipdb_country_name))
    logNotice("aipdb_country_code: " .. aipdb_country_code .. "| type: " .. type(aipdb_country_code))
    logNotice("aipdb_total_report: " .. aipdb_total_report .. "| type: " .. type(aipdb_total_report))
    logNotice("aipdb_distinct_report: " .. aipdb_distinct_report .. "| type: " .. type(aipdb_distinct_report))
    logNotice("aipdb_last_report: " .. aipdb_last_report .. "| type: " .. type(aipdb_last_report))
    logNotice("aipdb_time: " .. aipdb_time .. "| type: " .. type(aipdb_time))
    logNotice("----------------------END-------------------------")
    logNotice("==== END AIPDB cache IP info ====")

    if aipdb_ip == "api-down" then
        logNotice("Warning, AIPDB API is down detected")

        cmdstr = "mail -s '[Maxicata | Warning]: AIPDB API is down @ "
                .. hostname .. "' '" .. email .. "' < " .. maxicata_s_mail

        maxicata_s_mail_rpt:write("[Maxicata]: Maxicata has detected that AIPDB API call is down/offline")
        maxicata_s_mail_rpt:flush()
        if enable_mail_report == true then
            if enable_api_error_mail == true then
                -- This will send a lot of email if the downtime is long. So use this only for debugging
                -- disable enable_api_error_mail when in production or I will need to create mail by rate TODO
                logNotice("API mail error is enabled. Sending the log")
                io.popen_x(cmdstr)
                io.open(maxicata_s_mail, "w"):close()
            else
                logNotice("API mail error is disabled. No email is sent, please read the log instead")
            end
        else
            logNotice("Global email report is enabled")
        end
        log()  -- Just call back the log function -- stop everything and call this - restart (pass to next check) - TODO testing this
    end
end

function get_attack_category(ip)
    logNotice("=== START suspicious attack category ===")
    -- maxicata -c generate_susp_log <ip> or maxicata -c gsl <ip>
    -- This function will look for an IP for suspected log found in system, if found, it will then write a report for that IP for each log path,
    -- It also will categorize each log for reporting and this function return log category number in sequence to be used with AIPDB report.
    -- If no category found, it will return 0
    attack_category = io.popen_readline("maxicata -c 'get-attack-category' '" .. ip .. "'")
    logNotice("Suspicious log category is: " .. attack_category)
    logNotice("==== END log suspicious attack category ====")
    return attack_category
end

function get_susp_log_count()
    logNotice("=== START suspicious log count status ===")
    -- maxicata -c get-susp-log-count <ip> or maxicata -c gslc <ip>
    check_susp_log_count = io.popen_readline("maxicata -c get-susp-log-count")
    logNotice("Suspicious log count for IP " .. susp_ip .. " is " .. check_susp_log_count .. "\n")
    logNotice("==== END suspicious log count status ====")
    return check_susp_log_count
end

function get_modsecurity_status()
    logNotice("=== START mod security triggered status ===")
    check_modsec_status = io.popen_readline("maxicata -c is-mod-security-triggered")
    logNotice("Mod security trigger status for this IP " .. susp_ip .. " is " .. check_modsec_status .. "\n")
    logNotice("==== END suspicious log count status ====")
    return check_modsec_status
end

-- This function will scan IP for blacklist status from various RBL website using Maxicata wrapper
-- There is also a cache file to speed up checking in this script. The reason is to reduce the time scan.
-- If cache not found in database, we mark the scan in a file to set pending scan status (because if we wait, it would take 10-30 seconds)
function blacklist_scan(ip)
    logNotice("=== START blacklist_scan status ====")
    if enable_rbl_scan == true then
        logNotice("RBL-Blacklist scan is enabled")
        logNotice("Checking if IP is in blacklisted file ...")
        -- This function maxicata -c is-ip-blacklisted returns the blacklist count or nil (if no result)
        get_ip_blacklisted_cache_count = io.popen_readline("maxicata -c 'get-ip-blacklisted-cache-count' '" .. ip .. "'")

        if get_ip_blacklisted_cache_count == "not-found" then
            -- Meaning if the IP does not exist in blacklist database (not blacklisted),
            -- We run the blacklist scanner at the background
            -- cmdstr = "nohup bash maxicata -c scan-bl-q " .. ip .. "  </dev/null >/dev/null 2>&1 & -- (Temporarily not using background)
            logNotice("IP is not in blacklisted file. Scanning IP " .. ip .. " for blacklist (this may take sometime) ...")
            blacklist_count = io.popen_readline("maxicata -c 'rblscan' '" .. ip .. "'")
            blacklist_status = "New"
            -- blacklist_count = bl_count
            logNotice("New blacklist count is: " .. blacklist_count)
        else
            logNotice("IP is in blacklisted cached file")
            blacklist_status = "Cached"
            blacklist_count = get_ip_blacklisted_cache_count
        end
        logNotice("Cache blacklist count is: " .. get_ip_blacklisted_cache_count)

        bl_domain = io.popen_readline("maxicata -c 'get-blacklisted-cache-var' '" .. ip .. "' 'domain'")

        if bl_domain == "empty-var" then
            bl_domain = "None"
        end

        -- Fix if the blacklist_count has null value (especially coming from the scan) - the scan can finish without blacklist and return nil.
        if blacklist_count == "not-found" or blacklist_count == nil then
            logNotice("Blacklist count is 'not-found' or empty and will now set as 0")
            blacklist_count = 0
        end
    else
        logNotice("RBL-Blacklist scan is disabled")
    end
    logNotice("==== END blacklist_scan status =====")
end

-- This is an init function that is requires by Suricata to define what data need to display
function init()
    local needs = {}
    needs["type"] = "packet"
    needs["filter"] = "alerts"
    return needs
end

function setup()
    -- This means you don't have to manually decide what to do with the IP.
    enable_auto_action = true
    -- enable mail report (if disable it is recommend to enable auto action)
    enable_mail_report = true
    -- enable_api_error_mail (make sure enable_mail_report is enable for this to work)
    enable_api_error_mail = true
    -- enable whitelist ip_add
    enable_whitelist_ip = true
    -- enable ignore ip_add
    enable_ignore_ip = true
    -- enable suppress ip_add
    enable_suppress_ip = true
    -- Set enable RBL scan
    enable_rbl_scan = true
    -- Set to suppress msg
    enable_suppress_msg = true --or false
    -- enable_log is use to disable or enable log in suricata.log for debugging purpose:
    enable_log = true -- or false (Disable for good performance)
    -- enable_self_log is used to write log based only from lua script (ignore the suricata system log)
    enable_self_log = true -- or false. You need enable_log = true to use this (Disable for good performance)
    -- enable_csf_auto_block is use to set whether to auto block IP using CSF (through wrapper: maxicata -d <ip>)
    enable_csf_auto_block = true
    -- enable CIDR24 check
    enable_cidr_twentyfour = true
    ip_cidr_twentyfour_class = "Unknown CIDR24"
    ip_net_class = "Unknown"
    ip2cidr24 = "Unknown"
    ip_cidr_twentyfour_class_action = "No action for this"
    ip_cidr_twentyfour_count = "0"

    -- enable CIDR 16 check
    -- enable_cidr_sixteen = true
    --ip_cidr_sixteen_class = "Unknown"
    --ip_cidr_sixteen_class_action = "No action for this"
    --ip_cidr_sixteen_count = "0"

    -- send report to this email
     email = "arafat@sofibox.com"
    -- set the hostname
    hostname = "mars.codegix.com"
    -- Sample IP, put yours
    localipv4 = "1.1.1.2"
    localipv6 = "2400:8901:0000:0000:f13c:ff:fe05:6cb2"
    ip_aipdb_cache_duration = "3600" -- update the IP cache every 1 hour. After 1 hour, IP with over this timestamp will be removed from cache
    ip_aipdb_report_cache_duration = "900" -- in second (AIPDB limit 15 minutes per-report. 15 minutes = 900 seconds)
    aipdb_ip_cache_status = "N/A"
    aipdb_cached = "new" --or cached
    api_cached_desc = "API cached description is not available"
    alert = "n" -- or y
    alert_level = "Unknown Alert Level"
    alert_level_action = "Alert level action is not available"
    alert_cache_subject = "Alert cache subject is not available"

    -- Category to report in AbuseIPDB
    -- aipdb_enable_report is use to enable auto report that has bad score into AIPDB web database
    aipdb_enable_report = true
    -- maxicata_enable_report is use to enable auto report that has bad score into AIPDB web database
    maxicata_enable_report = true
    surCategory = "15" -- This is a default category in AIPDB (Hacking). Visit abuseipdb.com/categories for more category
    aipdb_report_desc = "AIPDB Report Description is not available"
    score_status = "N/A" -- ok or bad or unknown (Initial N/A)

    -- Initialize IP suppress list variable
    suppress_ip = "N/A"
    -- suppress_ip_sid = "N/A"
    -- suppress_ip_time = "N/A"

    -- Initialize IP for whitelist list variable
    whitelist_ip = "N/A"
    -- whitelist_ip_sid = "N/A"
    -- whitelist_ip_time = "N/A"

    -- Initialize message suppress list variable
    suppress_msg = "N/A"
    -- suppress_msg_time = "N/A"

    -- Initialize cached variables from AIPDB before function call
    susp_ip = "N/A"
    aipdb_ip = "N/A"
    aipdb_iswhitelisted = "N/A"
    aipdb_abuse_score = "0"
    aipdb_isp = "N/A"
    aipdb_usage_type = "N/A"
    aipdb_domain = "N/A"
    aipdb_country_name = "N/A"
    aipdb_country_code = "N/A"
    aipdb_total_report = "0"
    aipdb_distinct_report = "0"
    aipdb_last_report = "N/A"
    aipdb_time = "N/A"

    -- Blacklist and suspicious log count
    blacklist_count = 0
    blacklist_status = "pending scan" -- or blacklisted

    -- DUMMY file for debug only
    dummy = "dummy.log"

    -- The main Maxicata lua log (For debugging)
    maxicata_log = SCLogPath() .. "maxicata.log"
    maxicata_rpt = assert(io.open(maxicata_log, "a"))

    -- For sending program error, status mail
    maxicata_s_mail = SCLogPath() .. "maxicata-s-mail.log"
    maxicata_s_mail_rpt = assert(io.open(maxicata_s_mail, "a"))

    -- The main Maxicata lua log (accept both critical and non critical alert) (ok)
    maxicata_yn_log = SCLogPath() .. "maxicata-yn.log"
    maxicata_yn_rpt = assert(io.open(maxicata_yn_log, "a"))

    -- The main Maxicata alert for yes only (ok)
    maxicata_y_log = SCLogPath() .. "maxicata-y.log"
    maxicata_y_rpt = assert(io.open(maxicata_y_log, "a"))

    -- The generic mail log handler for quick reporting
    maxicata_y_mail = SCLogPath() .. "maxicata-y-mail.log"
    maxicata_y_mail_rpt = assert(io.open(maxicata_y_mail, "a"))

    -- The mail rate log reporting (for non critical alert) -raw
    maxicata_n_log = SCLogPath() .. "maxicata-n.log"
    maxicata_n_rpt = assert(io.open(maxicata_n_log, "a"))

    -- The mail rate log reporting (for non critical alert) - this one truncated/limited
    maxicata_n_limit_log = SCLogPath() .. "maxicata-n-limit.log"
    maxicata_n_limit_rpt = assert(io.open(maxicata_n_limit_log, "a"))

    -- The mail rate log reporting (for non critical alert) - normalized
    maxicata_n_mail = SCLogPath() .. "maxicata-n-mail.log"
    maxicata_n_mail_rpt = assert(io.open(maxicata_n_mail, "a"))

    -- For keeping history if an IP is already reported
    --reported_ip = SCLogPath() .. "maxicata-reported-ip.log"
    --reported_ip_rpt = assert(io.open(reported_ip, "a"))

    -- The temporary path for grep (Not using this anymore)
    -- greptmp = SCLogPath() .. "grep.temp"
    -- grep_temp = assert(io.open(greptmp, "a"))

    -- db_patch cache
    maxicata_db_path_cache = io.popen_readline("maxicata -c 'get-path' 'db-path-cache'")
    -- aipdb cache path
    aipdb_ip_cache_file = maxicata_db_path_cache .. "aipdb_ip.cache"
    -- aipdb report cache path:
    aipdb_ip_reported_cache_file = maxicata_db_path_cache .. "aipdb_ip_reported.cache"

    -- The suspicious log path from Maxicata (this one contain non-critical log)
    maxicata_log_path = io.popen_readline("maxicata -c 'get-path' 'log-path'")
    susp_log_path = maxicata_log_path .. "maxicata-SUSP_LOG_RPT.log"
    -- susp_log_path_rpt = assert(io.open(susp_log_path, "a"))

    -- Report count
    count = 0
    -- store non-critical alert count
    alert_count = 0
    -- non-critical alert rate to send email
    -- alert_rate = 3
    alert_rate = 50
end

function log()
    -- this will be destroy in every call

    -- CSF block status (need to initialize here, every call need to set no first)
    is_csf_blocked = "no"

    -- RBL Blacklist status (need to initialized here, every call need to set
    blacklist_status = "Unknown (ipv6)"

    bl_domain = "Unknown (ipv6)"
    -- Generate new value from suricata
    timestring = SCPacketTimeString()
    sid, rev, gid = SCRuleIds()
    msg = SCRuleMsg()
    class, priority = SCRuleClass()
    ip_version, src_ip, dst_ip, protocol, src_port, dst_port = SCPacketTuple()

    --Remove the decimal point for the result
    sid = math.floor(sid)
    rev = math.floor(rev)
    gid = math.floor(gid)
    priority = math.floor(priority)
    src_port = math.floor(src_port)
    dst_port = math.floor(dst_port)
    protocol = math.floor(protocol)
    ip_version = math.floor(ip_version)
    --  End remove decimal points

    if class == nil then
        class = "unknown"
    end

    if src_ip == localipv4 or src_ip == localipv6 then
        src_ip = "LOCAL_IP"
        susp_ip = dst_ip
    end

    if dst_ip == localipv4 or dst_ip == localipv6 then
        dst_ip = "LOCAL_IP"
        susp_ip = src_ip
    end
    -- 1) FOR WHITELIST
    whitelist_ip = io.popen_readline("maxicata -c 'get-whitelisted-ip-var' " .. susp_ip .. " 'ip_address'") -- readall because it can contains one or more | nope just use readline. next future readall
    -- TODO suppress based on SID condition
    -- TODO whitelist_sid = io.popen_readline("maxicata -c get-whitelisted-ip-var " .. ip .. " sid") --readall because it can contains one or more | nope just use readline. next future readall --THIS ONE NEXT FEATURE
    -- This one useful to clear suppression based on time: TODO whitelist_time = io.popen_readline("maxicata -c get-whitelisted-ip-var " .. ip .. " time") -- | nope just use readline. next future readall THIS ONE NEXT FEATURE
    -- If IP is not in the whitelist list (not in whitelist list = 1)

    -- 2) FOR IGNORE LIST
    ignore_ip = io.popen_readline("maxicata -c 'get-ignored-ip-var' " .. susp_ip .. " 'ip_address'") -- readall because it can contains one or more | nope just use readline. next future readall


    if (whitelist_ip ~= "empty-var" and enable_whitelist_ip == true) or (ignore_ip ~= "empty-var" and enable_ignore_ip == true) then
        local whitelist_ignore = "N/A"
        if whitelist_ip ~= "empty-var" then
            whitelist_ignore = "whitelist"
        elseif ignore_ip ~= "empty-var" then
            whitelist_ignore = "ignore"
        end
        --TODO need testing
        -- if there is an IP in whitelist and whitelist setting is enable then we don't log anything OR
        -- if there is an IP in ignore list and ignore list setting is enable then we don't log anything
        logNotice("IP : " .. susp_ip .. " is in " .. whitelist_ignore .. " file. Ignoring this IP from reporting in Suricata")
    else
        -- If IP is in whitelist we still log it because we need enable whitelist setting to true
        logNotice("IP : " .. susp_ip .. " is not in the whitelist list and ignore list and whitelist_ip and ignore_ip setting is enable")
        -- After got the susp_ip above, now we call the cache function for caching IP
        set_aipdb_cached_data(susp_ip)
        -- Here after call cache from aipdb above, now we scan the suspected IP for blacklist in the background:
        -- Only call this if susp_ip is ip version 4 because ipv6 is not working for blacklist scanning
        logNotice("IP_VERSION is: " .. ip_version)
        if ip_version == 4 then
            -- CIDR rule
            if enable_cidr_twentyfour == true then
                logNotice("CIDR24 is enabled")
                ip_cidr_twentyfour_count = io.popen_readline("maxicata -c 'cidr24-count' " .. susp_ip)
                logNotice("CIDR_COUNT is: " .. ip_cidr_twentyfour_count)
                if (tonumber(ip_cidr_twentyfour_count) == 0) then
                    ip_cidr_twentyfour_class = "Notice, IP has no block record in CSF permanent deny list"
                    ip_cidr_twentyfour_class_action = "No action for this"
                elseif (tonumber(ip_cidr_twentyfour_count) == 1) then
                    ip_cidr_twentyfour_class = "Notice, only 1 IP detected in CSF permanent deny list"
                    ip_cidr_twentyfour_class_action = "No action for this"
                elseif (tonumber(ip_cidr_twentyfour_count) >= 1) and (tonumber(ip_cidr_twentyfour_count) < 5) then
                    ip_cidr_twentyfour_class = "Notice, only " .. ip_cidr_twentyfour_count .. " blocked IPs detected in CSF permanent deny list"
                    ip_cidr_twentyfour_class_action = "No action for this"
                elseif (tonumber(ip_cidr_twentyfour_count) >= 5) then
                    ip2cidr24 = io.popen_readline("maxicata -c 'ip2cidr' '" .. susp_ip .. "' '24'")
                    ip_cidr_twentyfour_class = "Warning, found " .. ip_cidr_twentyfour_count .. "  blocked IPs in CSF permanent deny list. This IP might belong to a large group of suspicious network in CIDR24"
                    ip_cidr_twentyfour_class_action = "It's recommend to block this IP as CIDR form to increase CSF performance: " .. ip2cidr24
                else
                    ip_cidr_twentyfour_class = "Unknown CIDR24"
                    ip_cidr_twentyfour_class_action = "No action for this"
                end
            else
                ip_cidr_twentyfour_count = "0"
            end
            blacklist_scan(susp_ip)
        else
            ip_cidr_twentyfour_count = "0" -- set the default value for cidr count if ipv6
            blacklist_count = 0 -- set the default value for count if ipv6
            ip_cidr_twentyfour_class = "Unknown (ipv6) CIDR24"
            ip_cidr_twentyfour_class_action = "No action for this"
        end
        logNotice("The count and the class is: ")
        logNotice(ip_cidr_twentyfour_count)
        logNotice(ip_cidr_twentyfour_class)
        logNotice("The type for count and the class is: ")
        logNotice(type(ip_cidr_twentyfour_count))
        logNotice(type(ip_cidr_twentyfour_class))
        -- Now we mark this in the report if the result is cached or not
        if aipdb_cached == "new" then
            api_cached_desc = "New IP Info"
        else
            api_cached_desc = "Cached IP Info"
        end

        -- Search for suspicious log for this IP here:
        logNotice("Calling get_attack_category(susp_ip)")
        rpt_category = get_attack_category(susp_ip)
        logNotice("Calling get_susp_log_count()")
        susp_log_count = get_susp_log_count()
        logNotice("Calling get_modsecurity_status()")
        modsec_status = get_modsecurity_status()
        -- This is just fo debugging: (must remove this for production)
        --alert = "n"

        -- Write log to main report --
        -- Output format: |N 1 |ALERT: n |PRIO: 3 |TIME: 10/26/2020-15:26:55.098546 |IPLAYER: {6} 68.65.121.210 {: 2082 } ->
        -- LOCAL_IP {: 443 } |GSR: [:G 1 :S 2210008 :R 2 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA STREAM 3way handshake SYN resend different seq on SYN recv
        str_rpt = "|N " .. count .. " |ALERT: " .. alert .. " |PRIO: " .. priority .. " |TIME: " .. timestring ..
                " |IPLAYER: {" .. protocol .. "} " .. src_ip .. " {: " .. src_port ..
                " } -> " .. dst_ip .. " {: " .. dst_port .. " } |GSR: [:G " .. gid .. " :S " .. sid .. " :R " .. rev ..
                " ] |CLASS: " .. class .. " |MSG: " .. msg .. "\n"
        maxicata_yn_rpt:write(str_rpt)
        maxicata_yn_rpt:flush()
        -- sleep(1) -- sleep a little bit (nope)

        -- When you call AIPDB report to report the same IP, you will be notified by the API that you can only report the same IP within 15 minutes.
        -- So calling the report also increase usage limit. So this function fixed it:
        -- Usage: maxicata -c clear-ip-cache <file_input> <duration_in_seconds_ago>
        -- new code
        clear_ip_aipdb_status = io.popen_readline("maxicata -c 'clear-ip-info-cache' " .. ip_aipdb_cache_duration)
        clear_ip_aipdb_reported_status = io.popen_readline("maxicata -c 'clear-ip-reported-cache' " .. ip_aipdb_report_cache_duration)
        if priority == 1 then
            alert_level = "Very High Risk"
        elseif priority == 2 then
            alert_level = "High Risk"
        elseif priority == 3 then
            alert_level = "Medium Risk"
        elseif priority > 3 then
            -- 4, 5, 7, 8, 9 above
            alert_level = "Low Risk"
        end

        -- Get the source ip count Inbound
        src_ip_count = io.popen_readline("maxicata -c get_src_ip_count " .. susp_ip .. " " .. maxicata_yn_log)
        -- Get the destination ip count Outbound
        dst_ip_count = io.popen_readline("maxicata -c get_dst_ip_count " .. susp_ip .. " " .. maxicata_yn_log)
        -- Get both destination and source ip count (inbound and outbound)
        src_dst_count = tonumber(src_ip_count) + tonumber(dst_ip_count)
        -- Calculate to block
        -- Rule 1: If Priority Level is: Very High Risk (1) , High Risk (2), Medium Risk (3) AND abuse_score is >=80, then block.
        -- Here they tell me what is right and I block that IP (I don't report)
        if (tonumber(aipdb_abuse_score) >= 80) then
            logNotice("AIPDB rule is triggered, condition matched ")
            csf_execute_block("AIPDB Rule") -- this also will assign new alert_level_action
            score_status = "bad"
            aipdb_report_desc = "This IP has bad score reported from AIPDB, no need to report to AIPDB"

            -- end
            -- Rule 1: If priority >=3 AND blacklist count >=7  AND susp_log_count >=1, then need to report and block | 5 is the best number I think
            -- Rule 2: If priority = 1, then need to report and block (Very High Risk Rule by Suricata)
            -- Rule 3: If priority = 2, then need to report and block (High Risk Rule by Suricata)
            -- Here I tell them what is right, I report and block that IP so they can decide to block too. (now they also need to tell me a bit about the score)
        elseif (tonumber(blacklist_count) >= 5 and tonumber(susp_log_count) >= 1) or
                (tonumber(aipdb_abuse_score) >= 65 and
                        tonumber(blacklist_count) >= 2) or
                (tonumber(priority) == 1) or
                (tonumber(priority) == 2) or
                (modsec_status == "yes") then
            logNotice("Maxicata rule is triggered, condition matched ")
            score_status = "bad"
            aipdb_report_desc = "This IP has bad score, and has been set to report to AIPDB"
            csf_execute_block("Maxicata Rule") -- this also will assign new alert_level_action
        else
            logNotice("Cannot determine bad rule (Not enough score)")
            is_csf_ip_perm_blocked = ("maxicata -c 'is-csf-ip-perm-blocked' '" .. susp_ip .. "'")
            if is_csf_ip_perm_blocked == "true" then
                logNotice("IP has already blocked in CSF before")
                score_status = "bad"
                aipdb_report_desc = "This IP has bad score and permanently blocked by CSF before. It has been set to report to AIPDB"
                alert_level_action = "This IP has been permanently blocked by CSF but it has triggered one or more suspicious suricata stream packets. See 'Your Action' below how to suppress this IP"
            else
                -- If not able to determine bad score, then need to manually audit this IP.
                logNotice("IP has not been blocked in CSF before. So need to audit immediately")
                score_status = "unknown"
                aipdb_report_desc = "Unable to determine more bad score to report to AIPDB"
                alert_level_action = "Warning, unable to determine more reputation score for this IP. It is recommend to audit this IP manually"

            end

            -- when enable auto action is true and score status cannot be determined, we suppress this IP in non-critical report automatically
            if enable_auto_action == true and score_status == "unknown" and tonumber(susp_log_count) == 0 then
                logNotice("Auto Action is enabled and score status is unknown, suppressing IP")
                suppress_ip_status = io.popen_readline("maxicata -c 'suppress-ip' '" .. susp_ip .. "' '" .. sid .. "' '|GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/" .. aipdb_domain .. "/" .. aipdb_country_name .. "/" .. aipdb_country_code .. " | Abuse_Percentage: " .. aipdb_abuse_score .. "%" .. " #by-auto-action'")

                alert_level_action = "Warning, unable to determine more reputation score for this IP. IP has been set to suppress list because auto action is enabled. Status: [" .. suppress_ip_status .. "]"
            end

            -- when enable auto action is true and score status is bad and csf already block this, (bad IP still showing alert after blocked), then we put it in ignore list
            -- ignore list is for bad IP
            if enable_auto_action == true and score == "bad" and is_csf_ip_perm_blocked == "true" then
                logNotice("Auto Action is enabled, score status is bad and csf is blocked, ignoring IP")
                ignore_ip_status = io.popen_readline("maxicata -c 'ignore-ip' '" .. susp_ip .. "' '" .. sid .. "' '|GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/" .. aipdb_domain .. "/" .. aipdb_country_name .. "/" .. aipdb_country_code .. " | Abuse_Percentage: " .. aipdb_abuse_score .. "%" .. " #by-auto-action'")
                alert_level_action = "Warning, IP was blocked by CSF before but it's still showing alert. IP has been set to ignore list (for bad IPs) because auto action is enabled. Status: [" .. ignore_ip_status .. "]"

            end

        end
        -- Setting report here
        if score_status == "bad" and (aipdb_enable_report == true or maxicata_enable_report == true) then

            logNotice("The AIPDB report condition is triggered +!+!+!+")
            -- This one must follow AIPDB rule here for suricata: https://www.abuseipdb.com/suricata
            if string.match(class, "web application") then
                surCategory = "21" -- Web App Attack
            elseif string.match(class, "user") or string.match(class, "User") or string.match(class, "administrator") or string.match(class, "Administrator") then
                surCategory = "18" -- Bruteforce
            elseif string.match(class, "suspicious username") or string.match(class, "default username") then
                surCategory = "18,22" -- Bruteforce, SSH
            elseif (string.match(class, "rpc") or string.match(class, "Network scan") or string.match(class, "Information Leak")) then
                surCategory = "14" -- Port scan
            elseif string.match(class, "Denial of Service") then
                surCategory = "4" -- DDOS Attack
            elseif rpt_category ~= "0" then
                surCategory = rpt_category -- Use category from the log suspicious from Maxicata if it exist
            else
                surCategory = "15" -- If not above just use the default category, Hacking.
            end

            if string.match(SCRuleMsg(), "SQL INJECTION") then
                surCategory = surCategory .. ",16" -- Above category + SQL Injection
            end
            logNotice("surCategory is: " .. surCategory)
            -- Setting up comment:
            surComment = "[time]: " .. timestring .. " [level]: " .. alert_level .. " (" .. priority .. "/5) [rbl_blacklist]: (" .. blacklist_count .. ")" ..
                    bl_domain .. " [warn_log]: " .. susp_log_count .. " log(s) [abuse_cat]: " .. surCategory ..
                    " [rule_score]: " .. score_status .. " [inbound]: (" .. src_ip_count .. ") + [outbound]: (" .. dst_ip_count .. ")=" ..
                    src_dst_count .. " time(s) [last_stream]: " .. src_ip .. ":" .. src_port ..
                    " => " .. dst_ip .. ":" .. dst_port .. " [class]: " .. class .. " [activity]: " .. msg
            -- TODO suspicious_log_message variable to be put in surComment
            -- Before report, we need to check the reported_ip file if timestamp expired, then we remove it
            logNotice("aipdb report is enabled. Sending report to AIPDB ...")
            -- Just use readline not readall, we only take the first error line
            err_status = io.popen_readline("maxicata -c 'report-to-aipdb' '" .. susp_ip .. "' '" .. surCategory .. "' '" .. surComment .. "'")
            aipdb_report_desc = err_status
        else
            aipdb_report_desc = "Found no bad score and no report to send to AIPDB"
        end

        if (src_dst_count == 0 or src_dst_count == 1) then
            -- This means that it has history of suspicious activity
            alert_cache_subject = "New"
        else
            alert_cache_subject = "Existing"
        end

        -- Here we can modify the alert based on specific condition to non-critical alert to reduce report. For example Search Engine Spider.
        -- If score is bad and csf already block then we don't need to alert, we put it in non-critical alert

        if score_status ~= "ok" then
            alert = "y"
        end

        --if aipdb_iswhitelisted == "true" then
        -- if aipdb reported this IP is whitelist, then we should put it as non-critical or is_csf_blocked ~= "yes"
        --    alert = "n"
        --end


        -- START Define suppress IP and message here
        -- 1) For suppress IP
        suppress_ip = io.popen_readline("maxicata -c 'get-suppressed-ip-var' " .. susp_ip .. " 'ip_address'") -- readall because it can contains one or more | nope just use readline. next future readall
        -- TODO suppress based on condition SID:
        -- TODO suppress_sid = io.popen_readline("maxicata -c get-suppressed-ip-var " .. ip .. " sid") --readall because it can contains one or more | nope just use readline. next future readall --THIS ONE NEXT FEATURE
        -- # This one useful to clear suppression based on time: TODO suppress_time = io.popen_readline("maxicata -c get-suppressed-ip-var " .. ip .. " time") -- | nope just use readline. next future readall THIS ONE NEXT FEATURE
        -- If IP is in suppression list (which is not equal to 1)
        if enable_suppress_ip == true then
            if suppress_ip ~= "empty-var" then
                logNotice("IP : " .. susp_ip .. " is in the supression list. Putting this IP as non-critical IP")
                alert = "n"
            end
        else
            logNotice("suppression IP list is not enable")
        end


        -- 2) For suppress msg
        suppress_msg = io.popen_readline("maxicata -c 'get-suppressed-msg-var' '" .. msg .. "' 'msg'")

        if enable_suppress_msg == true then
            logNotice("Msg suppression list is enabled")
            if suppress_msg ~= "empty-var" then
                logNotice("Suppress_msg: " .. suppress_msg)
                logNotice("Message suppression [" .. msg .. " is in the supression list. Putting this rule in non-critical report")
                alert = "n"
            end
        else
            logNotice("suppression message list is not enable")
        end
        -- END suppress IP and message

        -- IF CSF already block this IP, put it on alert = "n"

        -- This for force the report to become non-critical [just for debugging] --
        -- alert = "n" -- for debug only comment to remove
        -- TODO Suggest only action that is recommended based on score and hide others. For example don't suggest to whitelist IP if IP has 100% bad score.
        if (ip_version == 4 and tonumber(ip_cidr_twentyfour_count) >= 5) then
            ip2cidr24 = io.popen_readline("maxicata -c 'ip2cidr' '" .. susp_ip .. "' '24'")
            ip_net_class = "network"
            -- Add 2 argument one network Ip and suspect IP, maxicata will determine how to block and when blocking using this CIDR, we can report that susp_ip at AIPDB
            cmd_man_blockstr = "maxicata -dip " .. ip2cidr24 .. " 'Suricata_CSF_Manual_Block |GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                    .. aipdb_domain .. "/" .. aipdb_country_code .. " |Abuse_Percentage:" .. aipdb_abuse_score
                    .. "% |Alert_Level: " .. alert_level .. " |RBL_Blacklisted_Count: " .. blacklist_count .. " |Inbound_Outbound: " .. src_dst_count .. " # do not delete'"

        else
            ip_net_class = "single"
            cmd_man_blockstr = "maxicata -dip " .. susp_ip .. " 'Suricata_CSF_Manual_Block |GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                    .. aipdb_domain .. "/" .. aipdb_country_code .. " |Abuse_Percentage:" .. aipdb_abuse_score
                    .. "% |Alert_Level: " .. alert_level .. " |RBL_Blacklisted_Count: " .. blacklist_count .. " |Inbound_Outbound: " .. src_dst_count .. " # do not delete'"
        end

        suppress_ip_cmd = "maxicata -sip " .. susp_ip .. " " .. sid .. " '|GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_name .. "/" .. aipdb_country_code .. " |Abuse_Percentage: " .. aipdb_abuse_score .. "%" .. "'"
        whitelist_ip_cmd = "maxicata -wip " .. susp_ip .. " " .. sid .. " '|GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_name .. "/" .. aipdb_country_code .. " |Abuse_Percentage: " .. aipdb_abuse_score .. "%" .. "'"

        ignore_ip_cmd = "maxicata -iip " .. susp_ip .. " " .. sid .. " '|GeoIP: " .. aipdb_isp .. "/" .. aipdb_usage_type .. "/"
                .. aipdb_domain .. "/" .. aipdb_country_name .. "/" .. aipdb_country_code .. " |Abuse_Percentage: " .. aipdb_abuse_score .. "%" .. "'"

        suppress_msg_cmd = "maxicata -smsg '" .. msg .. "'"
        if (alert == "y") then
            logNotice("ALERT y condition is triggered ++_+_++ ...")
            -- Only call API if alert is y -- good for API limit usage
            -- Call the AIPDB data - dont run if previously already call the same ip to prevent API limit usage
            logNotice("Creating report for critical maxicata-y.log ...")
            y_rpt_str = "|n " .. count .. " |alert: " .. alert .. " |time: " .. timestring .. " |critical_rpt: y |prio: " .. priority ..
                    " |score: " .. score_status .. " |rbl_count: " .. blacklist_count .. " |susp_log_count: " .. susp_log_count ..
                    " |is_csf_blocked: " .. is_csf_blocked .. " |aipdb_iswhitelisted: " .. aipdb_iswhitelisted ..
                    " |aipdb_score: " .. aipdb_abuse_score .. " |ip_layer: {" .. protocol .. "} " .. src_ip .. " {: " .. src_port ..
                    " } -> " .. dst_ip .. " {: " .. dst_port .. " } |GSR: [:G " .. gid .. " :S " .. sid .. " :R " .. rev ..
                    " ] |class: " .. class .. " |msg: " .. msg .. "\n"
            maxicata_y_rpt:write(y_rpt_str)
            maxicata_y_rpt:write("\n")
            maxicata_y_rpt:flush()

            maxicata_y_mail_rpt:write("Warning, new suspicious IP from [" .. susp_ip .. "/" .. aipdb_country_name .. "]" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "AbuseIPDB Security Info [" .. api_cached_desc .. "]" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Abuse Confidence Score: " .. aipdb_abuse_score .. "%" .. "\n"
                    .. "IP: " .. aipdb_ip .. "\n"
                    .. "is IP whitelisted: " .. aipdb_iswhitelisted .. "\n"
                    .. "ISP: " .. aipdb_isp .. "\n"
                    .. "Usage Type: " .. aipdb_usage_type .. "\n"
                    .. "Domain Name: " .. aipdb_domain .. "\n"
                    .. "Country Name/Code: " .. aipdb_country_name .. "/" .. aipdb_country_code .. "\n"
                    .. "AIPDB Report Information: " .. "\n"
                    .. "*---*" .. "\n"
                    .. "- This IP address has been reported a total of " .. aipdb_total_report .. " time(s) from " .. aipdb_distinct_report
                    .. " distinct sources. " .. susp_ip .. " most recent report was [" .. aipdb_last_report .. "]. More info at: https://www.abuseipdb.com/check/" .. susp_ip .. "\n"
                    .. "- " .. aipdb_report_desc .. "\n"
                    .. "- " .. clear_ip_aipdb_status .. "\n"
                    .. "- " .. clear_ip_aipdb_reported_status .. "\n"
                    .. "*---*" .. "\n"
                    .. "\n")
            maxicata_y_mail_rpt:flush()
            -- need to separate report write like this if not I will get this error: too many C levels (limit is 200) in function at line lua.
            maxicata_y_mail_rpt:write("--------------------- " .. "\n"
                    .. "Suricata Security Info:" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Risk Priority Level: " .. alert_level .. " (" .. priority .. "/5)" .. "\n"
                    .. "Source/Inbound(" .. src_ip_count .. ") + Destination/Outbound(" .. dst_ip_count .. ") = " .. src_dst_count .. " record(s)" .. "\n"
                    .. "MaxiRBL Scan Status: " .. blacklist_status .. " with " .. blacklist_count .. " record(s) blacklisted" .. "\n"
                    .. "MaxiRBL Blacklisted Domain: " .. bl_domain .. "\n"
                    .. "IP CIDR24 Count: " .. ip_cidr_twentyfour_class .. " with " .. ip_cidr_twentyfour_count .. " record(s)" .. "\n"
                    .. "Suspicious Log Count: " .. susp_log_count .. " log location(s)" .. "\n"
                    .. "Modsecurity Alert: " .. modsec_status .. "\n"
                    .. "AIPDB Attack Categorized Code: " .. surCategory .. " [More info at: http://abuseipdb.com/categories]" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Info & Recommend Action: " .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "1) " .. ip_cidr_twentyfour_class .. ". " .. ip_cidr_twentyfour_class_action .. "\n"
                    .. "2) " .. alert_level_action .. "\n"
                    .. "3) See `Your Action` below to manually block, suppress or whitelist this IP" .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Your Action: " .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "1) Block this " .. ip_net_class .. " IP using CSF: " .. "\n"
                    .. "---" .. "\n"
                    .. "\n"
                    .. cmd_man_blockstr .. "\n"
                    .. "\n"
                    .. "---" .. "\n"
                    .. "2) Suppress this IP and put as non-critical report:" .. "\n"
                    .. "\n"
                    .. suppress_ip_cmd .. "\n"
                    .. "\n"
                    .. "3) Suppress this type of suspicious attack msg and put as non-critical report:" .. "\n"
                    .. "\n"
                    .. suppress_msg_cmd .. "\n"
                    .. "\n"
                    .. "---" .. "\n"
                    .. "4) Whitelist this IP from reporting in suricata:" .. "\n"
                    .. "\n"
                    .. whitelist_ip_cmd .. "\n"
                    .. "\n"
                    .. "5) Ignore this IP from reporting in suricata:" .. "\n"
                    .. "\n"
                    .. ignore_ip_cmd .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Suspicious IP web references:" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "1) https://www.abuseipdb.com/check/" .. susp_ip .. "\n"
                    .. "2) https://infobyip.com/ip-" .. susp_ip .. ".html" .. "\n"
                    .. "3) https://stopforumspam.com/ipcheck/" .. susp_ip .. "\n"
                    .. "4) https://www.spamhaus.org/query/ip/" .. susp_ip .. "\n"
                    .. "5) https://talosintelligence.com/reputation_center/lookup?search=" .. susp_ip .. "\n"
                    .. "6) http://www.borderware.com/lookup.php?ip=" .. susp_ip .. "\n"
                    .. "7) https://www.ipqualityscore.com/ip-reputation-check/lookup/" .. susp_ip .. "\n"
                    .. "8) http://" .. susp_ip .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Current suspicious activity from this IP" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. str_rpt .. "\n"
                    .. "---" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "\n"
                    .. "-----------------" .. "\n"
                    .. "History of suspicious activities from this IP with [alert=yes]:" .. "\n"
                    .. "-----------------" .. "\n")
            maxicata_y_mail_rpt:flush()
            --   # maxicata -c grep-ip-from-file-q <ip_add> <input_file> <mandatory_output_file>
            io.popen_x("maxicata -c grep-ip-from-file-q '" .. susp_ip .. "' '" .. maxicata_y_log .. "' '" .. maxicata_y_mail .. "'")
            maxicata_y_mail_rpt:write("---" .. "\n"
                    .. "\n"
                    .. "-----------------" .. "\n"
                    .. "History of suspicious activities from this IP with [alert=no]:" .. "\n"
                    .. "--------------------- " .. "\n")
            maxicata_y_mail_rpt:flush()
            --   # maxicata -c grep-ip-from-file-q <ip_add> <input_file> <mandatory_output_file>
            io.popen_x("maxicata -c grep-ip-from-file-q '" .. susp_ip .. "' '" .. maxicata_n_log .. "' '" .. maxicata_y_mail .. "'")
            maxicata_y_mail_rpt:write("---" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "Other suspicious log files from this IP: [" .. susp_ip .. "/" .. aipdb_country_name .. "]:" .. "\n"
                    .. "--------------------- " .. "\n")
            maxicata_y_mail_rpt:flush()
            io.popen_x("cat " .. susp_log_path .. " >> " .. maxicata_y_mail)
            maxicata_y_mail_rpt:write("---" .. "\n"
                    .. "--------------------- " .. "\n"
                    .. "\n")
            maxicata_y_mail_rpt:flush()
            logNotice("Sending critical report email alert (maxicata-y.log ...")
            cmdstr = "mail -s '[Maxicata | " .. alert_level .. " Alert (" .. score_status .. " score | aipdb: " .. aipdb_abuse_score .. "%) ]: " .. src_dst_count .. " " .. alert_cache_subject .. " record(s) from IP [" .. susp_ip .. "/" .. aipdb_country_name .. "] @ "
                    .. hostname .. "' '" .. email .. "' < " .. maxicata_y_mail
            if enable_mail_report == true then
                io.popen_x(cmdstr)
            end
            io.open(maxicata_y_mail, "w"):close() -- clear the log file
        else
            -- if alert = n (non critical)
            logNotice("ALERT n condition is triggered ++_+_++ ...")
            alert_count = alert_count + 1
            -- Just show list of non critical alert from this file, but we need to store this first at above inside maxicata_n_log... so dont use maxicata_log -- done
            -- insert the last line into the non critical-report file to be used later
            logNotice("Creating maxicata-n.log report ...")
            n_rpt_str = "|n " .. count .. " |alert: " .. alert .. " |time: " .. timestring .. " |critical_rpt: n |prio: " .. priority ..
                    " |score: " .. score_status .. " |rbl_count: " .. blacklist_count .. " |susp_log_count: " .. susp_log_count ..
                    " |is_csf_blocked: " .. is_csf_blocked .. " |aipdb_iswhitelisted: " .. aipdb_iswhitelisted ..
                    " |aipdb_score: " .. aipdb_abuse_score .. " |ip_layer: {" .. protocol .. "} " .. src_ip .. " {: " .. src_port ..
                    " } -> " .. dst_ip .. " {: " .. dst_port .. " } |GSR: [:G " .. gid .. " :S " .. sid .. " :R " .. rev ..
                    " ] |class: " .. class .. " |msg: " .. msg .. "\n"
            maxicata_n_rpt:write(n_rpt_str)
            maxicata_n_rpt:write("\n")
            maxicata_n_rpt:flush()

            -- Make copy for maxicata_n_limit_log (this one special for non critical report which should be cleared automatically once reported.
            maxicata_n_limit_rpt:write(n_rpt_str)
            maxicata_n_limit_rpt:write("\n")
            maxicata_n_limit_rpt:flush()

            if alert_count == alert_rate then
                -- it's time to display the noncritical report if reached this count
                -- Display each IP and also count, so it's easier to see if IP is distinct. (done)
                alert_count = 0 -- reset back to 0 to re-count
                -- Calculate the non-critical ip using maxicata (bash)
                maxicata_n_mail_rpt:write("Suricata has collected non-critical activities with detection rate of " .. alert_rate .. " times" .. "\n"
                        .. "---------" .. "\n"
                        .. "\n")
                maxicata_n_mail_rpt:flush()
                -- current correct field: <src_ip_fld> <dst_ip_fld> <last_record_sid> 21 26 34 (Now 25 30 38)
                logNotice("Critical log report has reached limit to send notification. Calculating non-critical IP abuse information ...")
                io.popen_x("maxicata -c 'report-non-critical-ip' '" .. maxicata_n_limit_log .. "' 25 30 38 >> '" .. maxicata_n_mail .. "'")
                maxicata_n_mail_rpt:write("\n"
                        .. "---------" .. "\n")
                maxicata_n_mail_rpt:flush()
                if enable_mail_report == true then
                    -- Send email from the above file after it has reached threshold of alert_rate value
                    logNotice("Sending non critical alert email ...")
                    io.popen_x("mail -s '[Maxicata | Rate: " .. alert_rate .. "]: Non-Critical IP Report From Suricata @ " .. hostname .. "' '" .. email .. "' < " .. maxicata_n_mail)
                end -- clear log file of maxicata_n_mail because we don't need to keep this as this file is just a temporary file to handle mail.
                logNotice("Clear both n limit log and n mail files ...")
                io.open(maxicata_n_limit_log, "w"):close()
                io.open(maxicata_n_mail, "w"):close()
            end
        end
        -- count total record each time suricata detects new suspicious activity (both critical and non-critical)
        count = count + 1

    end
end

function deinit()
    -- Close all report files
    logNotice("Maxicata Reports Logged: " .. count);
    io.close(maxicata_rpt)
    io.close(maxicata_yn_rpt)
    io.close(maxicata_y_rpt)
    io.close(maxicata_y_mail_rpt)
    io.close(maxicata_n_rpt)
    io.close(maxicata_n_limit_rpt)
    io.close(maxicata_n_mail_rpt)
    --io.close(grep_temp)
    --io.close(susp_log_path_rpt)
end

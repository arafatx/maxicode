# 1) maxigpg --help
Usage information

```
MaxiGPG Documentation

Author: Arafat Ali | Email: arafat@sofibox.com

Usage:
maxigpg [<options>] OPTIONAL [ -k  <KEY_ID> [ OPTIONAL <cron_warn> ]]

options:

  -h, --help
      This help text.

  -v, --version
      Show version information

  -i, --init
      Remove all configuration, passphrases, stop gpg-agent process
      and recreate new configuration files (init default setting)

  -cp, --clearpass
      Clear all gpg-agent passphrases

  -x, --stop
      Stop gpg-agent process. This will also clear the passphrase

  -s, --status OPTIONAL <KEY_ID>
      eg: maxigpg -s OPTIONAL <KEY_ID>
      Show gpg-agent process and cache status for a given key ID. KEY_ID is an optional argument
      If no KEY_ID argument is given, it will then find the current valid passphrase with its cached KEY_ID
      If KEY_ID argument is empty and not in cache file, program will halt for correct KEY_ID.

  -rconf, --removeconf
      This will remove gpg-agent configuration files. The new configuration will be created
      automatically when the script is running again

  -k, --set, --setpass, --setpassword, --cache, --key <KEY_ID> OPTIONAL<cron_warn>
      eg: maxigpg -k <KEY_ID> <cron_warn>
      eg: maxigpg -k 2B705B8B6FA943B1 --cron-warn-only
```

# a) -i | --init  

Use this command to initialize the program in default state

```
[maxigpg]: Removing config files ...
[maxigpg]: Ok, config files have been removed
[maxigpg]: Stopping gpg-agent and clear all passphrases cached from gpg-agent ...
[maxigpg]: OK, gpg-agent has been stopped!
[maxigpg]: Writing new GPG config file at /usr/local/maxicode/maxigpg/conf/gpg-agent.conf ...
-------
allow-preset-passphrase
default-cache-ttl 34560000
max-cache-ttl 34560000
-------

[maxigpg]: Writing new config file at /usr/local/maxicode/maxigpg/conf/gpg-main.conf ...
-------
date_cached=1606623622
process_id='-1'
expired_in=34560000
-------

[root@earth maxigpg]#
```

# b) -cp | --clearpass
clear all passphrase from gpg-agent

If gpg-agent is running:
```
[maxigpg]: Forgetting all passphrases from gpg-agent ...
[maxigpg]: OK
```

If gpg-agent is not running:
```
[maxigpg]: Forgetting all passphrases from gpg-agent ...
[maxigpg]: Warning, no passphrase to clear because gpg-agent is not running!
```

# c) -x | --stop

```
[root@earth maxigpg]# maxigpg --stop
[maxigpg]: Stopping gpg-agent and clear all passphrases cached from gpg-agent ...
[maxigpg]: OK, gpg-agent has been stopped!
```
# d) -k | --key <key_id>

This is the first time when you execute the following command:

```
[root@earth maxigpg]# maxigpg --key
[maxigpg]: Error, missing <key_id>. e.g: maxigpg --key 2B705B8B6FA943B1
[root@earth maxigpg]# maxigpg --key 2B705B8B6FA943B1
[maxigpg]: Warning, no gpg-agent daemon is running!
[maxigpg]: Starting gpg-agent daemon with config from [/usr/local/maxicode/maxigpg/conf/gpg-agent.conf] ...

---------------------------
--- (1) PID: 783954 ---
Process ID details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
 783954  21532 root     root     gpg-agent       gpg-agent --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon
Process TREE details:
systemd,1 --switched-root --system --deserialize 17
  `-gpg-agent,783954 --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon

Warning, current gpg-agent PID (1)[783954] does not match with the cached gpg-agent PID [-1]
---------------------------
[maxigpg]: OK, gpg-agent daemon is now running with the following process id(s): [783954]
[maxigpg]: OK, current gpg-agent PID [783954] is now matched with the cached gpg-agent PID [783954]
[maxigpg]: Warning, no cached passphrase is detected in gpg-agent for key ID of [2B705B8B6FA943B1]
[maxigpg]: Enter a passphrase to cache into a running gpg-agent [783954]:
[maxigpg]: Now, validating the cached passphrase input ...
-------
[maxigpg]: OK, a passphrase is valid in gpg-agent [783954]
[maxigpg]: The passphrase is valid with the following information:
------------------------------
Valid from: [Sun Nov 29 12:37:36 +08 2020]
Expired on: [Mon Jan  3 12:37:36 +08 2022]
Expired in: [400d 0h 0m 0s]
------------------------------
[maxigpg]: Status is [OK]
[maxigpg]: Overall status is [OK]
[root@earth maxigpg]#:
```
If a passphrase is valid and already cached before you don't have to cache again (You can also cache multiple keys):

```
[root@earth maxigpg]# maxigpg --key 2B705B8B6FA943B1
[maxigpg]: OK, gpg-agent daemon is already running with the following process ID(s):

---------------------------
--- (1) PID: 783954 ---
Process ID details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
 783954 168996 root     root     gpg-agent       gpg-agent --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon
Process TREE details:
systemd,1 --switched-root --system --deserialize 17
  `-gpg-agent,783954 --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon

OK, current gpg-agent PID (1)[783954] is matched with the cached PID [783954]
---------------------------
[maxigpg]: OK, a passphrase is already cached in gpg-agent [783954] for key ID of [2B705B8B6FA943B1]
[maxigpg]: Now, validating the cached passphrase input ...
-------
[maxigpg]: OK, a passphrase is valid in gpg-agent [783954]
[maxigpg]: The passphrase is valid with the following information:
------------------------------
Valid from: [Sun Nov 29 12:37:36 +08 2020]
Expired on: [Mon Jan  3 12:37:36 +08 2022]
Expired in: [399d 23h 54m 0s]
------------------------------
[maxigpg]: Status is [OK]
[maxigpg]: Overall status is [OK]
[root@earth maxigpg]#

```

If you provide wrong key which does not exist
```
[root@earth maxigpg]# maxigpg --key 2B705B8B6FA943
[maxigpg]: OK, gpg-agent daemon is already running with the following process ID(s):

---------------------------
--- (1) PID: 783954 ---
Process ID details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
 783954 168996 root     root     gpg-agent       gpg-agent --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon
Process TREE details:
systemd,1 --switched-root --system --deserialize 17
  `-gpg-agent,783954 --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon

OK, current gpg-agent PID (1)[783954] is matched with the cached PID [783954]
---------------------------
[maxigpg]: Warning, Found error: [ gpg: error reading key: No secret key ]
[maxigpg]: usage: ./maxigpg -k <key_id>
[root@earth maxigpg]#
```

# e) -s | --status
Show gpg-agent process and cache status for a given key ID (gpg-agent needs to run first)

If no gpg-agent is running:

```
[root@earth maxigpg]# maxigpg --status 2B705B8B6FA943B1
[maxigpg]: Warning, no gpg-agent is running.
```

If gpg-agent is running but no passphrase or invalid passphrase is in cache:

```
[root@earth maxigpg]# maxigpg --status 2B705B8B6FA943B1
[maxigpg]: OK, gpg-agent daemon is already running with the following process ID(s):

---------------------------
--- (1) PID: 782314 ---
Process ID details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
 782314  95264 root     root     gpg-agent       gpg-agent --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon
Process TREE details:
systemd,1 --switched-root --system --deserialize 17
  `-gpg-agent,782314 --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon

OK, current gpg-agent PID (1)[782314] is matched with the cached PID [782314]
---------------------------
[maxigpg]: Warning, no valid cache passphrase found for 2B705B8B6FA943B1.
[maxigpg]: Validation return status:
*------*
gpg: signing failed: Bad passphrase
gpg: signing failed: Bad passphrase
*------*
[root@earth maxigpg]#
```

If gpg-agent is running with a valid passphrase

```
maxigpg --status 2B705B8B6FA943B1
[maxigpg]: OK, gpg-agent daemon is already running with the following process ID(s):

---------------------------
--- (1) PID: 782314 ---
Process ID details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
 782314 168996 root     root     gpg-agent       gpg-agent --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon
Process TREE details:
systemd,1 --switched-root --system --deserialize 17
  `-gpg-agent,782314 --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon

OK, current gpg-agent PID (1)[782314] is matched with the cached PID [782314]
---------------------------
[maxigpg]: OK, passphrase is valid for 2B705B8B6FA943B1.
[maxigpg]: Validation return status:
*------*
OK
*------*
```

If KEY_ID is not supplied and you want to know the status of all KEY (currently support only 1 key for checking this):

```
[root@earth maxigpg]# maxigpg --status
[maxigpg]: Found KEY_ID [2B705B8B6FA943B1] in cache. Checking 2B705B8B6FA943B1 status ...
[maxigpg]: OK, gpg-agent daemon is already running with the following process ID(s):

---------------------------
--- (1) PID: 4185656 ---
Process ID details:
    PID MEMORY USER     GROUP    COMMAND         ARGS
4185656 169016 root     root     gpg-agent       gpg-agent --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon
Process TREE details:
systemd,1 --system --deserialize 20
  `-gpg-agent,4185656 --options /usr/local/maxicode/maxigpg/conf/gpg-agent.conf --daemon

OK, current gpg-agent PID (1)[4185656] is matched with the cached PID [4185656]
---------------------------
[maxigpg]: OK, passphrase is valid for 2B705B8B6FA943B1.
[maxigpg]: Validation return status:
*------*
OK
*------*

```



This is Maxicata, I implement this to monitor suricata log file and notify via email if somebody is doing suspicious activity (in realtime). The notification rules are triggered from suricata official and some 3rd parties rules (like RBLScanner, AbuseIPDB, Suspicious log files via regex). These are the (best) extra suricata rules that I use with zero false positive so far:

```
[root@earth conf]# /home/****/maxipy/bin/suricata-update list-enabled-sources
14/10/2020 -- 13:06:50 - <Info> -- Using data-directory /var/lib/suricata.
14/10/2020 -- 13:06:50 - <Info> -- Using Suricata configuration /etc/suricata/suricata.yaml
14/10/2020 -- 13:06:50 - <Info> -- Using /usr/share/suricata/rules for Suricata provided rules.
14/10/2020 -- 13:06:50 - <Info> -- Found Suricata version 6.0.0 at /usr/bin/suricata.
Enabled sources:
  - sslbl/ssl-fp-blacklist
  - ptresearch/attackdetection
  - etnetera/aggressive
  - oisf/trafficid
  - sslbl/ja3-fingerprints
  - https://gitlab.com/cybergon/timon-rules/raw/master/cybergon.rules
  - et/open
[root@earth conf]#

```

Just run this for the first time:

```
  maxicata --initfull

```

Sample output if bad IP detected doing a suspicious activity, we will receive an email like this:

```
[Maxicata | Medium Risk Alert (bad score | aipdb: 100%) ]: 1 New record(s) from IP [212.70.149.5/Bulgaria] @ earth.sofibox.com 
Warning, new suspicious IP from [212.70.149.5/Bulgaria]
---------------------
AbuseIPDB Security Info [New IP Info]
---------------------
Abuse Confidence Score: 100%
IP: 212.70.149.5
is IP whitelisted: false
ISP: Global Communication Net Plc
Usage Type: Fixed Line ISP
Domain Name: gcn.bg
Country Name/Code: Bulgaria/BG
AIPDB Report Information:
*---*
- This IP address has been reported a total of 22079 time(s) from 209 distinct sources. 212.70.149.5 most recent report was [2020-12-05T20:44:45+00:00]. More info at: https://www.abuseipdb.com/check/212.70.149.5
- Ok, 212.70.149.5 has been reported successfully into AIPDB web. View this report at: https://www.abuseipdb.com/212.70.149.5
- Notice, 1 IP [42.234.116.105] has been removed from aipdb_ip.cache
- Notice, 3 IPs [ 2403:6a40:0000:0123:0aa9:0b82:8da8:8cc9 212.70.149.84 212.70.149.21] have been removed from aipdb_ip_reported.cache
*---*

---------------------
Suricata Security Info:
---------------------
Risk Priority Level: Medium Risk (3/5)
Source/Inbound(1) + Destination/Outbound(0) = 1 record(s)
MaxiRBL Scan Status: New with 17 record(s) blacklisted
MaxiRBL Blacklisted Domain: black.junkemailfilter.com, hostkarma.junkemailfilter.com, noptr.spamrats.com, bl.blocklist.de, bl.fmb.la, all.s5h.net, dnsbl-1.uceprotect.net, ips.backscatterer.org, cbl.abuseat.org, b.barracudacentral.org, bb.barracudacentral.org, dnsbl.justspam.org, pbl.spamhaus.org, sbl-xbl.spamhaus.org, xbl.spamhaus.org, zen.spamhaus.org, free.v4bl.org
IP CIDR24 Count: Notice, only 2 blocked IPs detected in CSF permanent deny list with 2 record(s)
Suspicious Log Count: 6 log location(s)
Modsecurity Alert: no
AIPDB Attack Categorized Code: 11,18 [More info at: http://abuseipdb.com/categories]
---------------------
Info & Recommend Action:
---------------------
1) Notice, only 2 blocked IPs detected in CSF permanent deny list. No action for this
2) CSF has already blocked this IP before
3) See `Your Action` below to manually block, suppress or whitelist this IP

---------------------
Your Action:
---------------------
1) Block this single IP using CSF:
---

maxicata -dip 212.70.149.5 'Suricata_CSF_Manual_Block |GeoIP: Global Communication Net Plc/Fixed Line ISP/gcn.bg/BG |Abuse_Percentage:100% |Alert_Level: Medium Risk |RBL_Blacklisted_Count: 17 |Inbound_Outbound: 1 # do not delete'

---
2) Suppress this IP and put as non-critical report:

maxicata -sip 212.70.149.5 2260002 '|GeoIP: Global Communication Net Plc/Fixed Line ISP/gcn.bg/Bulgaria/BG |Abuse_Percentage: 100%'

3) Suppress this type of suspicious attack msg and put as non-critical report:

maxicata -smsg 'SURICATA Applayer Detect protocol only one direction'

---
4) Whitelist this IP from reporting in suricata:

maxicata -wip 212.70.149.5 2260002 '|GeoIP: Global Communication Net Plc/Fixed Line ISP/gcn.bg/Bulgaria/BG |Abuse_Percentage: 100%'

5) Ignore this IP from reporting in suricata:

maxicata -iip 212.70.149.5 2260002 '|GeoIP: Global Communication Net Plc/Fixed Line ISP/gcn.bg/Bulgaria/BG |Abuse_Percentage: 100%'

---------------------
Suspicious IP web references:
---------------------
1) https://www.abuseipdb.com/check/212.70.149.5
2) https://infobyip.com/ip-212.70.149.5.html
3) https://stopforumspam.com/ipcheck/212.70.149.5
4) https://www.spamhaus.org/query/ip/212.70.149.5
5) https://talosintelligence.com/reputation_center/lookup?search=212.70.149.5
6) http://www.borderware.com/lookup.php?ip=212.70.149.5
7) https://www.ipqualityscore.com/ip-reputation-check/lookup/212.70.149.5
8) http://212.70.149.5

---------------------
Current suspicious activity from this IP
---------------------
|N 131 |ALERT: n |PRIO: 3 |TIME: 12/06/2020-04:45:47.736073 |IPLAYER: {6} 212.70.149.5 {: 59820 } -> LOCAL_IP {: 25 } |GSR: [:G 1 :S 2260002 :R 1 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA Applayer Detect protocol only one direction

---
---------------------

-----------------
History of suspicious activities from this IP with [alert=yes]:
-----------------
|n 131 |alert: y |time: 12/06/2020-04:45:47.736073 |critical_rpt: y |prio: 3 |score: bad |rbl_count: 17 |susp_log_count: 6 |is_csf_blocked: yes |aipdb_iswhitelisted: false |aipdb_score: 100 |ip_layer: {6} 212.70.149.5 {: 59820 } -> LOCAL_IP {: 25 } |GSR: [:G 1 :S 2260002 :R 1 ] |class: Generic Protocol Command Decode |msg: SURICATA Applayer Detect protocol only one direction
---

-----------------
History of suspicious activities from this IP with [alert=no]:
---------------------
---
---------------------

---------------------
Other suspicious log files from this IP: [212.70.149.5/Bulgaria]:
---------------------
1) CSF PERMANENT IP BAN LOCATION - BLOCKED BY CSF
[maxicata]: Found this IP at: /etc/csf/csf.deny:
---------
[maxicata]: Latest 10 records:
---
212.70.149.5 # lfd: (smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs - Sun Dec  6 03:08:30 2020
---------

2) CSF TEMPORARY IP BAN LOCATION
[maxicata]: Found this IP at: /var/lib/csf/csf.tempip:
---------
[maxicata]: Latest 10 records:
---
212.70.149.5|1|1607119781|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607119843|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607126270|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607145844|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607157258|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607158459|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607160869|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607161498|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607161858|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
212.70.149.5|1|1607195310|(smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs
---------

3) CSF TEMPORARY IP BAN LOCATION 2 - MOSTLY BY DA
[maxicata]: Found this IP at: /var/lib/csf/csf.tempban:
---------
[maxicata]: Latest 10 records:
---
1607129586|212.70.149.5|25|inout|157680000|Blocked port 25 with Directadmin Brute Force Manager
1607129587|212.70.149.5|465|inout|157680000|Blocked port 465 with Directadmin Brute Force Manager
1607129587|212.70.149.5|587|inout|157680000|Blocked port 587 with Directadmin Brute Force Manager
---------

4) LFD LOG FILE (Login Failed Daemon log)
[maxicata]: Found this IP at: /var/log/lfd.log:
---------
[maxicata]: Latest 10 records:
---
Dec  6 03:08:31 earth lfd[1436229]: (smtpauth) Failed SMTP AUTH login from 212.70.149.5 (GB/United Kingdom/-): 1 in the last 3600 secs - *Blocked in csf* [LF_TRIGGER]
---------

5) DIRECTADMIN BLOCKED IP LOCATION
[maxicata]: Found this IP at: /root/blocked_ips.txt:
---------
[maxicata]: Latest 10 records:
---
212.70.149.5=dateblocked=1607129587
---------

6) EXIM MAIL MAIN LOG
[maxicata]: Found this IP at: /var/log/exim/mainlog:
---------
[maxicata]: Latest 10 records:
---
2020-12-06 03:08:28 login authenticator failed for (User) [212.70.149.5]: 535 Incorrect authentication data (set_id=promethe@******.com)
2020-12-06 03:13:28 SMTP command timeout on connection from (User) [212.70.149.5]
2020-12-06 04:00:28 SMTP command timeout on connection from [212.70.149.5]
2020-12-06 04:38:24 SMTP command timeout on connection from [212.70.149.5]
---------

7) EXIM MAIL REJECT LOG
[maxicata]: Found this IP at: /var/log/exim/rejectlog:
---------
[maxicata]: Latest 10 records:
---
2020-12-06 03:08:28 login authenticator failed for (User) [212.70.149.5]: 535 Incorrect authentication data (set_id=promethe@sofibox.com)
---------

8) SURICATA FAST LOG
[maxicata]: Found this IP at: /var/log/suricata/fast.log:
---------
[maxicata]: Latest 10 records:
---
12/06/2020-04:45:47.736073  [**] [1:2260002:1] SURICATA Applayer Detect protocol only one direction [**] [Classification: Generic Protocol Command Decode] [Priority: 3] {TCP} 212.70.149.5:59820 -> 172.104.50.185:25
---------

SUSP_LOG_COUNT: 6
MOD_SEC_STATUS: no (might disabled for certain webs)
---
---------------------


```

# Even AIPDB cannot determine the bad score (the worst is they put this IP in whitelist) but Maxicata can detect and block this IP. For example this so called 'trusted network' Facebook Search Engine type server is an expoited host by an attacker and use to send spam to one of my domains server. Don't ever trust whitelist.

```
[Maxicata | Medium Risk Alert (bad score | aipdb: 0%) ]: 1 New record(s) from IP [66.220.144.225/United States of America] @ earth.sofibox.com 

Warning, new suspicious IP from [66.220.144.225/United States of America]
---------------------
AbuseIPDB Security Info [New IP Info]
---------------------
Abuse Confidence Score: 0%
IP: 66.220.144.225
is IP whitelisted: unknown
ISP: Facebook Inc.
Usage Type: Search Engine Spider
Domain Name: facebook.com
Country Name/Code: United States of America/US
AIPDB Report Information:
*---*
1) This IP address has been reported a total of 0 time(s) from 0 distinct sources. 66.220.144.225 most recent report was [unknown]. More info at: https://www.abuseipdb.com/check/66.220.144.225
2) Ok, 66.220.144.225 has been reported successfully with no error

3) Notice, no IP is removed from REPORTED IP cache at this moment
*---*

---------------------
Suricata Security Info:
---------------------
Risk Priority Level: Medium Risk (3/5)
Source/Inbound(0) + Destination/Outbound(1) = 1 record(s)
MaxiRBL Scan Status: new with 8 record(s) blacklisted
MaxiRBL Blacklisted domain: 0spam-n.fusionzero.com, hostkarma.junkemailfilter.com, nobl.junkemailfilter.com, reputation-ip.rbl.scrolloutf1.com, db.wpbl.info, rep.mailspike.net, ix.dnsbl.manitu.net, score.spfbl.net
IP CIDR24 count: Notice, IP has no block record in CSF permanent deny list with 0 record(s)
Suspicious Log Count: 1
Modsecurity alert: no
AIPDB Attack Categorized Code: 11 [More info at: http://abuseipdb.com/categories]
---------------------
Info & Recommend Action:
---------------------
1) Notice, IP has no block record in CSF permanent deny list. No action for this
2) Adding 66.220.144.225 to csf.deny and iptables DROP...
3) See `Your Action` below to manually block, suppress or whitelist this IP

---------------------
Your Action:
---------------------
1) Block this single IP using CSF:
---

maxicata -dip 66.220.144.225 'Suricata_CSF_Manual_Block |GeoIP: Facebook Inc./Search Engine Spider/facebook.com/US |Abuse_Percentage:0% |Alert_Level: Medium Risk |RBL_Blacklisted_Count: 8 |Inbound_Outbound: 1 # do not delete'

---
2) Suppress this IP and put as non-critical report:

maxicata -sip 66.220.144.225 2260002 '|GeoIP: Facebook Inc./Search Engine Spider/facebook.com/United States of America/US |Abuse_Percentage: 0%'

3) Suppress this type of suspicious attack msg and put as non-critical report:

maxicata -smsg 'SURICATA Applayer Detect protocol only one direction'

---
4) Whitelist this IP from reporting in suricata:

maxicata -wip 66.220.144.225 2260002 '|GeoIP: Facebook Inc./Search Engine Spider/facebook.com/United States of America/US |Abuse_Percentage: 0%'

5) Ignore this IP from reporting in suricata:

maxicata -iip 66.220.144.225 2260002 '|GeoIP: Facebook Inc./Search Engine Spider/facebook.com/United States of America/US |Abuse_Percentage: 0%'

---------------------
Suspicious IP web references:
---------------------
1) https://www.abuseipdb.com/check/66.220.144.225
2) https://infobyip.com/ip-66.220.144.225.html
3) https://stopforumspam.com/ipcheck/66.220.144.225
4) https://www.spamhaus.org/query/ip/66.220.144.225
5) https://talosintelligence.com/reputation_center/lookup?search=66.220.144.225
6) http://www.borderware.com/lookup.php?ip=66.220.144.225
7) https://www.ipqualityscore.com/ip-reputation-check/lookup/66.220.144.225
8) http://66.220.144.225

---------------------
Current suspicious activity from this IP
---------------------
|N 3419 |ALERT: n |PRIO: 3 |TIME: 12/03/2020-06:28:28.691857 |IPLAYER: {6} LOCAL_IP {: 25 } -> 66.220.144.225 {: 43557 } |GSR: [:G 1 :S 2260002 :R 1 ] |CLASS: Generic Protocol Command Decode |MSG: SURICATA Applayer Detect protocol only one direction

---
---------------------

-----------------
History of suspicious activities from this IP with [alert=yes]:
-----------------
|n 3419 |alert: y |time: 12/03/2020-06:28:28.691857 |critical_rpt: y |prio: 3 |score: bad |rbl_count: 8 |susp_log_count: 1 |is_csf_blocked: no |aipdb_iswhitelisted: unknown |aipdb_score: 0 |ip_layer: {6} LOCAL_IP {: 25 } -> 66.220.144.225 {: 43557 } |GSR: [:G 1 :S 2260002 :R 1 ] |class: Generic Protocol Command Decode |msg: SURICATA Applayer Detect protocol only one direction
---

-----------------
History of suspicious activities from this IP with [alert=no]:
---------------------
---
---------------------

---------------------
Other suspicious log files from this IP: [66.220.144.225/United States of America]:
---------------------
1) EXIM MAIL MAIN LOG
[maxicata]: Found this IP at: /var/log/exim/mainlog:
---------
[maxicata]: Latest 10 records:
---
2020-12-03 06:28:29 H=66-220-144-225.mail-pages.facebook.com [66.220.144.225] X=TLS1.3:TLS_AES_256_GCM_SHA384:256 CV=no F=<no-reply@mail.instagram.com> rejected RCPT <sunagokkagany3212@*.com>:
2020-12-03 06:28:30 H=66-220-144-225.mail-pages.facebook.com [66.220.144.225] incomplete transaction (RSET) from <no-reply@mail.instagram.com>
---------

2) EXIM MAIL REJECT LOG
[maxicata]: Found this IP at: /var/log/exim/rejectlog:
---------
[maxicata]: Latest 10 records:
---
2020-12-03 06:28:29 H=66-220-144-225.mail-pages.facebook.com [66.220.144.225] X=TLS1.3:TLS_AES_256_GCM_SHA384:256 CV=no F=<no-reply@mail.instagram.com> rejected RCPT <sunagokkagany3212@*.com>:
---------

3) SURICATA FAST LOG
[maxicata]: Found this IP at: /var/log/suricata/fast.log:
---------
[maxicata]: Latest 10 records:
---
12/03/2020-06:28:28.691857  [**] [1:2260002:1] SURICATA Applayer Detect protocol only one direction [**] [Classification: Generic Protocol Command Decode] [Priority: 3] {TCP} 172.104.50.185:25 -> 66.220.144.225:43557
---------

SUSP_LOG_COUNT: 1
MOD_SEC_STATUS: no (might disabled for certain webs)
---
---------------------
```

# Pending documentation


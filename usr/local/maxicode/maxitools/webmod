#!/bin/bash
# -------------------------------------------------
# Author: Arafat Ali | Email: webmaster@sofibox.com
# This script was created to put specific user website into maintenance mode when running a system backup using Directadmin
# Create web_config.ini [optional] to specify type of website eg. wordpress or prestashop or magento and other sites that support .htaccess
# If you don't create web_config.ini, the script will create a generic config template.
# This is very useful when doing a site backup and put all websites in a maintenance mode
# so users don't disturb database that can cause database corrupt during system backup
# Support a domain that has multiple type of websites stored in different subfolders.
# usage webmod --set user --offline-mode | --online-mode
# This command will put all domains including the subfolder applications of a specific user into maintenance or live mode.
# TODO 1: Ask for website type (eg: prestashop) and admin panel (eg: /admin_area) before creating default web_config.ini (currently need to manually edit this file)
function usage() {
  echo "${APP_SPECIFIC_NAME} Documentation"
  echo
  echo "Author: Arafat Ali | Email: arafat@sofibox.com"
  echo
  echo "Usage: "
  echo "${SCRIPT_NAME} [<options>] user <web_mod>"
  echo
  echo "options:"
  echo
  echo "  -h, --help"
  echo "      This help text."
  echo
  echo "  -v, --version"
  echo "      Show version information"
  echo

}

APP_SPECIFIC_NAME="WebMod"
SCRIPT_NAME=$(basename -- "$0")
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
CALLER_SCRIPT=$(ps -o comm= $PPID)
ADMIN_EMAIL="webmaster@sofibox.com"
LOG_PATH="${SCRIPT_PATH}/log"
CODE_PATH="${SCRIPT_PATH}/code"
CONFIG_PATH="${SCRIPT_PATH}/conf"
BACKUP_PATH="${SCRIPT_PATH}/backup"
mkdir -p "${LOG_PATH}"
mkdir -p "${CODE_PATH}"
mkdir -p "${CONFIG_PATH}"
mkdir -p "${BACKUP_PATH}"
DATE_BIN=$(command -v date)
RANDSTR="$(${DATE_BIN} '+%d-%m-%Y_%H-%M-%S').${RANDOM}"
REPORT_FILE="${LOG_PATH}/${SCRIPT_NAME}-${RANDSTR}.log"
CALLER_SCRIPT_LOG="${LOG_PATH}/${SCRIPT_NAME}-called-by-${CALLER_SCRIPT}-${RANDSTR}.log"
BOX_HOSTNAME=$(hostname)
MAIL_BIN=$(command -v mail)
MAINTENANCE_MODE="N/A"
WARN_STATUS="OK"
# Remove older log that is 1 day old
find "${LOG_PATH}" -name "*.log" -mtime +1 -exec rm {} \;

ARGNUM="$#"
# Handle option arguments
if [ $ARGNUM -eq 0 ]; then
  echo "[${SCRIPT_NAME}]: Error, no argument is supplied. Use --help to see the valid options"
fi

while [ "$#" -gt 0 ]; do
  case "$1" in
  # Display help and usage
  -h | --help)
    usage
    exit 0
    ;;
  -v | --version) # Display Program version
    echo "v0.2 - ${APP_SPECIFIC_NAME} by MaXi32 (Arafat Ali - sofibox.com)"
    exit 0
    break
    ;;
  -set | --set)
    shift
    web_user=$1
    maintenance_status=$2
    maintenance_time=$3
    running_mode=$4
    # Add extra postfix for security (sample put your random string here-for_filename)
    secure_postfix="backup_920839js982D_do_not_delete"
    # Declare subfolders to be checked
    # normally you declare common subfolder that contains another type of web application to be put in maintenance mode such as '/wordpress'
    # folders that dont exist for that users will be skip.
    declare -a web_subfolders=('' '/www' '/forums' '/code' '/test' '/wifi' '/dev')
    # Directadmin defined domain list for specific users:
    web_domains=$(cat "/usr/local/directadmin/data/users/${web_user}/domains.list")
    #If the domain list is empty  (If don't specify this function, it will also break in $check_web_subfolder) because the array domains.list is empty
    # If the web domain list is empty, we just notify in report
    if [ -z "${web_domains}" ]; then
      echo "[${SCRIPT_NAME}]: [Skipped] The user [${web_user}] does not have any domain records" | tee -a "${REPORT_FILE}"
      #exit 1 # Don't exit this because this one will break DA backup function. Alternatively use check_web_subfolder
    fi
    for web_domain in ${web_domains}; do
      for web_subfolder in "${web_subfolders[@]}"; do
        # Skip subfolder that does not exist from the web_subfolders declaration
        # This also will skip if users don't have domain created
        # DirectAdmin fact: No domain created meaning there is no subfolder inside domains folder
        check_web_subfolder="/home/${web_user}/domains/${web_domain}/public_html/${web_subfolder}"
        if ! [ -d "${check_web_subfolder}" ]; then
          #echo "[${SCRIPT_NAME}]: [Skipped] The user [${web_user}] does not have subfolder ${check_web_subfolder}" | tee -a "${REPORT_FILE}"
          continue
        fi
        if [ -z "${web_subfolder}" ]; then
          web_subfolder_info="/"
        else
          web_subfolder_info="${web_subfolder}"
        fi
        # Define web_config.ini location
        web_config="/home/${web_user}/domains/${web_domain}/public_html${web_subfolder}/web_config.ini"
        # If web_config.ini is found, then we read the config file
        if [ -e "$web_config" ]; then
          #echo "[${SCRIPT_NAME}]: [web_config.ini] found at $web_config. Loading web_config setting ..." | tee -a $REPORT_FILE
          # shellcheck disable=SC1090
          source "${web_config}"
          # Variables of web_config can be obtain here
        else
          echo "[${SCRIPT_NAME}]: Warning, [web_config.ini] does not exist in ${web_config}" | tee -a "${REPORT_FILE}"
          {
            echo "web_type='general'"
            echo "admin_path=''"
          } >>"${web_config}"
          chown "${web_user}":"${web_user}" "${web_config}"
          chmod 644 "${web_config}"
          echo "[${SCRIPT_NAME}]: Ok, a new [web_config.ini] file has been created in ${web_config}. Loading web_config setting ..." | tee -a "${REPORT_FILE}"
          # shellcheck disable=SC1090
          source "${web_config}"
          # Variables of web_config can be obtain here (blank)
        fi # Define maintenance page

        # If maintenance_time variable is set and it is an integer, we set the html
        if [[ -n "${maintenance_time}" && "${maintenance_time}" == '^[0-9]+$' ]]; then
          auto_approximate_notice="<p>The approximate time to finish this maintenance is less than ${maintenance_time} second(s)</p>"
        fi
        # This is the default htaccess for users
        root_htaccess_file="/home/${web_user}/domains/${web_domain}/public_html${web_subfolder}/.htaccess"
        # if the htaccess file exist, we look for the keyword MAXICODE_WEBMOD_CRON
        if [[ -f "${root_htaccess_file}" ]]; then
          maintenance_mode_root="$(head -1 "${root_htaccess_file}" | grep 'MAXICODE_WEBMOD_CRON')"
        else
          echo "[${SCRIPT_NAME}]: Warning, the main .htaccess file does not exist in the location specified in [${root_htaccess_file}]" | tee -a "${REPORT_FILE}"
          # No need to warn because I create htaccess for you
          #WARN_STATUS="WARNING"
          # Create a blank .htaccess
          echo "#" >"${root_htaccess_file}"
          chown "${web_user}":"${web_user}" "${root_htaccess_file}"
          chmod 644 "${root_htaccess_file}"
          echo "[${SCRIPT_NAME}]: OK, a blank .htaccess file has been created in ${root_htaccess_file}" | tee -a "${REPORT_FILE}"
          #$MAIL_BIN -s "[${WARN_STATUS}: $web_domain] Maintenance Operation Report" ${ADMIN_EMAIL} < $REPORT_FILE
        fi

        maintenance_page="/home/${web_user}/domains/${web_domain}/public_html${web_subfolder}/maintenance.html"
        if [[ "${maintenance_status}" == "turn-of-web" || "${maintenance_status}" == "--turn-off-web" || "${maintenance_status}" == "--turn-off" || "${maintenance_status}" == "off" || "${maintenance_status}" == "--offline" || "$maintenance_status" == "--offline-mode" ]]; then
          echo "[${SCRIPT_NAME}]: Turning on maintenance mode for ${web_user}'s website [${web_domain}] inside [${web_subfolder_info}]" | tee -a "${REPORT_FILE}"
          #echo "[${SCRIPT_NAME}]: Website type for $web_domain$web_subfolder is: $web_type" | tee -a $REPORT_FILE
          if [ -z "${maintenance_mode_root}" ]; then # If the htaccess doesn't have maintenance content (empty grep) we will set maintenance mode
            cat <<EOT >"${maintenance_page}"
            <!DOCTYPE html>
            <html>
                <!--
                This is an auto generated maintenance page by maxicode WebMod cron
                Author: Arafat Ali | Email: webmaster@sofibox.com
                -->
                <head>
                    <meta http-equiv="Refresh" content="5; url='http://${web_domain}'" />
                    <title>${web_domain}-Website Maintenance</title>
                    <style>
                        body { text-align: center; padding: 150px; }
                        h1 { font-size: 50px; }
                        body { font: 20px Helvetica, sans-serif; color: #333; }
                        article { display: block; text-align: left; width: 650px; margin: 0 auto; }
                        a { color: #dc8100; text-decoration: none; }
                        a:hover { color: #333; text-decoration: none; }
                    </style>
                </head>
            <body>
            <article>
                <h1>We&rsquo;ll be back soon!</h1>
                <div>
                <p>Sorry for the inconvenience but we&rsquo;re performing a site maintenance at the moment. If you need to you can always <a href="mailto:${ADMIN_EMAIL}">contact us</a>, otherwise <strong>${web_domain}</strong> will be online shortly!</p>
                $auto_approximate_notice
                <p><small>Maintenance started on: $(date)</small></p>
                <p>&mdash; The Team at ${web_domain}</p>
                </div>
            </article>
            </body>
            </html>
EOT
            chmod 644 "${maintenance_page}"
            chown "${web_user}":"${web_user}" "${maintenance_page}"
            # first rename the .htaccess as .htaccess_{secure_postfix}
            mv "${root_htaccess_file}" "${root_htaccess_file}_htaccess_webmod_x_${secure_postfix}"
            # Create new htaccess file in root
            touch "${root_htaccess_file}"
            # then write some maintenance contents
            {
              echo "# == AUTO GENERATED BY MAXICODE_WEBMOD_CRON =="
              echo "# This htaccess puts website ${web_domain} into maintenance mode"
              echo "RewriteEngine on"
              echo "RewriteCond %{REQUEST_URI} !/maintenance.html$ [NC]"
              echo "RewriteRule .* /maintenance.html [R=302,L]"
              echo "<filesMatch '.(htaccess|htpasswd|ini|log|sh|bak)$'>"
              echo "Order Allow,Deny"
              echo "Deny from all"
              echo "</filesMatch>"
            } >>"${root_htaccess_file}"
            # make sure the file permission are correct to that users in the web
            chmod 644 "${root_htaccess_file}"
            chown "${web_user}":"${web_user}" "${root_htaccess_file}"
            # pause a little bit to save energy, electricity and to prevent brain damage.
            sleep 1

            # shellcheck disable=SC2154
            # Prestashop v: 1.7.6.8
            if [ "${web_type}" == "prestashop" ]; then
              echo "[${SCRIPT_NAME}]: The website of [${web_domain}] is detected as prestashop type based on web_config.ini" | tee -a "${REPORT_FILE}"

              # DO PRESTASHOP THING
              # The default maintenance mode for prestashop backend doesn't block admin login. So when doing backup, somebody in the admin side can still change database.
              # In order to prevent that, we create a new .htaccess in that admin path defined by webconfig.ini
              # I have discussed this issue here: https://www.prestashop.com/forums/topic/1029258-any-alternative-ways-to-put-prestashop-into-maintenace-mode/?tab=comments#comment-3256264
              # for prestashop maintenance file
              ps_maintenance_htaccess_admin="/home/${web_user}/domains/${web_domain}/public_html${web_subfolder}${admin_path}/.htaccess"
              if [[ -f "${ps_maintenance_htaccess_admin}" ]]; then
                ps_maintenance_mode_admin="$(head -1 "${ps_maintenance_htaccess_admin}" | grep '== AUTO GENERATED BY MAXICODE_WEBMOD_CRON ==')"
              else
                echo "[${SCRIPT_NAME}]: The prestashop admin .htaccess file does not exist in the location specified in [${ps_maintenance_htaccess_admin}]" | tee -a "${REPORT_FILE}"
                WARN_STATUS="WARNING"
                echo "#" >"${ps_maintenance_htaccess_admin}"
                # shellcheck disable=SC2086
                chown "${web_user}":"${web_user}" "${ps_maintenance_htaccess_admin}"
                chmod 644 "${ps_maintenance_htaccess_admin}"
                echo "[${SCRIPT_NAME}]: A blank .htaccess file has been created in ${ps_maintenance_htaccess_admin}" | tee -a "${REPORT_FILE}"
                #$MAIL_BIN -s "[${WARN_STATUS}: $web_domain] Maintenance Operation Report" ${ADMIN_EMAIL} < $REPORT_FILE
                #exit 1
              fi
              #######################################
              # copy current htaccess inside admin_dash into .htaccess_y
              if [ -z "${ps_maintenance_mode_admin}" ]; then # If the htaccess doesn't have maintenance content we will set maintenance mode
                mv "${ps_maintenance_htaccess_admin}" "${ps_maintenance_htaccess_admin}_htaccess_webmod_y_${secure_postfix}"
                touch "${ps_maintenance_htaccess_admin}"
                # then write some maintenance contents
                {
                  echo "# == AUTO GENERATED BY MAXICODE_WEBMOD_CRON =="
                  echo "# This htaccess puts prestashop admin panel into maintenance mode"
                  echo "RewriteEngine on"
                  echo "RewriteCond %{REQUEST_URI} !/maintenance.html$ [NC]"
                  echo "RewriteRule .* /maintenance.html [R=302,L]"
                } >>"${ps_maintenance_htaccess_admin}"

                # Setting permission to the original state;
                chmod 644 "${ps_maintenance_htaccess_admin}"
                chown "${web_user}":"${web_user}" "${ps_maintenance_htaccess_admin}"
              fi
            elif [ "${web_type}" == "wordpress" ]; then
              # DO EXTRA WORDPRESS THING
              :
            elif [ "${web_type}" == "joomla" ]; then
              :
            elif [ "${web_type}" == "drupal" ]; then
              :
            elif [ "${web_type}" == "magento" ]; then
              :
            elif [ "${web_type}" == "opencart" ]; then
              :
            elif [ "${web_type}" == "laravel" ]; then
              :
            fi
            echo "[${SCRIPT_NAME}]: OK, the ${web_user}'s website of [${web_domain}] inside [${web_subfolder_info}] is now under maintenance mode" | tee -a "${REPORT_FILE}"
            MAINTENANCE_MODE="MAINTENANCE"
          else
            echo "[${SCRIPT_NAME}]: The ${web_user}'s website of [${web_domain}] inside [${web_subfolder_info}] is already in maintenance mode" | tee -a "${REPORT_FILE}"
            MAINTENANCE_MODE="MAINTENANCE"
          fi

        fi

        if [[ "${maintenance_status}" == "on" || "${maintenance_status}" == "--online" || "${maintenance_status}" == "--turn-on-web" || "${maintenance_status}" == "--live-mode" || "${maintenance_status}" == "--online-mode" ]]; then

          # maintenance status = 1 (TURN ON WEBSITE)
          ##
          echo "[${SCRIPT_NAME}]: Turning off maintenance mode for ${web_user}'s website [${web_domain}] inside [${web_subfolder_info}]" | tee -a "${REPORT_FILE}"
          if [ -z "${maintenance_mode_root}" ]; then # If the htaccess doesn't have maintenance content we will set maintenance mode
            echo "[${SCRIPT_NAME}]: The website of [${web_domain}] inside [${web_subfolder_info}] is not in maintenance mode" | tee -a "${REPORT_FILE}"
            MAINTENANCE_MODE="LIVE"
          else
            # copy the backup .htaccess_x into .htaccess (and overwrite)
            cp -f -p "${root_htaccess_file}_htaccess_webmod_x_${secure_postfix}" "${root_htaccess_file}"
            chmod 644 "${root_htaccess_file}"
            chown "${web_user}":"${web_user}" "${root_htaccess_file}"

            if [ "${web_type}" == "prestashop" ]; then
              echo "[${SCRIPT_NAME}]: The website of [${web_domain}] is detected as prestashop type based on web_config.ini" | tee -a "${REPORT_FILE}"

              # PS
              # copy the admin_path .htaccess_y backup into .htaccess inside that admin_dash folder (and overwrite)
              ps_maintenance_htaccess_admin="/home/${web_user}/domains/${web_domain}/public_html${web_subfolder}${admin_path}/.htaccess"
              if [ -f "${ps_maintenance_htaccess_admin}_htaccess_webmod_y_${secure_postfix}" ]; then
                cp -f -p "${ps_maintenance_htaccess_admin}_htaccess_webmod_y_${secure_postfix}" "${ps_maintenance_htaccess_admin}"
                chmod 644 "${ps_maintenance_htaccess_admin}"
                chown "${web_user}":"${web_user}" "${ps_maintenance_htaccess_admin}"
              else
                echo "[${SCRIPT_NAME}]: Warning the backup file of .htaccess_y_${secure_postfix} at .. domains/${web_domain}/public_html${web_subfolder}${admin_path}/ is missing" | tee -a "${REPORT_FILE}"
                exit 1
              fi
            elif [ "${web_type}" == "wordpress" ]; then
              # WP
              :
            elif [ "${web_type}" == "joomla" ]; then
              :
            elif [ "${web_type}" == "drupal" ]; then
              :
            elif [ "${web_type}" == "magento" ]; then
              :
            elif [ "${web_type}" == "opencart" ]; then
              :
            elif [ "${web_type}" == "laravel" ]; then
              :
            fi
            echo "[${SCRIPT_NAME}]: OK, user ${web_user}'s website of [${web_domain}] inside [${web_subfolder_info}] is now under live mode" | tee -a "${REPORT_FILE}"
            MAINTENANCE_MODE="LIVE"
          fi
          ##
        fi

        if [ -z "${running_mode}" ]; then
          running_mode="manual"
        fi

        echo "[${SCRIPT_NAME}]: Status: ${WARN_STATUS}" | tee -a "${REPORT_FILE}"
        echo "-------" | tee -a "${REPORT_FILE}"
        if [[ "${CALLER_SCRIPT}" == "user_backup_pre" || "${CALLER_SCRIPT}" == "user_backup_pos" ]]; then
          cat "${REPORT_FILE}" >>"${CALLER_SCRIPT_LOG}"
        fi
        if [ ${WARN_STATUS} == "WARNING" ]; then
          $MAIL_BIN -s "[${web_domain} inside ${web_subfolder_info} is in ${MAINTENANCE_MODE} mode | ${running_mode}] Web Mod Status @ ${BOX_HOSTNAME}" ${ADMIN_EMAIL} <"${REPORT_FILE}"
        fi
      done
    done
    exit 0
    break
    ;;
  --)
    break
    ;;
  -*)
    opt="${opt}"
    echo "Invalid option '${opt}'. Use --help to see the valid options"
    exit 1
    ;;
  # an option argument, continue
  *) ;;
  esac
  shift
done

# 1) maxiaide --help

```
MaxiAIDE Documentation

Usage:
maxiaide [options] OPTIONAL < --cronjob >...

options:

  -h, --help
      This help text.

  -s, --scan OPTIONAL <-c, --cronjob>
      Run file scan.
```


# 2) maxiaide -s, --scan OPTIONAL < -c, --cronjob >

```
[root@earth maxiaide]# maxiaide --scan --cronjob
-------
[maxiaide]: ---= FILE CHECK AND UPDATE START =---
[maxiaide]: Scanning file integrity ... (this may take some time):
Start timestamp: 2020-11-30 16:47:39 +0800 (AIDE 0.16)
AIDE found differences between database and filesystem!!
New AIDE database written to /usr/local/maxicode/maxiaide/db/aide.db.new.gz

Summary:
  Total number of entries:      216902
  Added entries:                4
  Removed entries:              0
  Changed entries:              2

---------------------------------------------------
Added entries:
---------------------------------------------------

f++++++++++++++++: /usr/local/maxicode/maxiaide/log/maxiaide-30-11-2020_16-47-39.6120-30-11-2020_16-47-39-report.log
f++++++++++++++++: /usr/local/maxicode/maxigpg/log/maxigpg-30-11-2020_16-47-01.5644-30-11-2020_16-47-01-report.log
f++++++++++++++++: /usr/local/maxicode/maxigpg/log/maxigpg-30-11-2020_16-48-01.25290-30-11-2020_16-48-01-report.log
f++++++++++++++++: /usr/local/maxicode/maxigpg/log/maxigpg-30-11-2020_16-49-01.26453-30-11-2020_16-49-01-report.log

---------------------------------------------------
Changed entries:
---------------------------------------------------

f   ...     C..  : /usr/local/maxicode/maxiaide/db/aide.db.gz
f   ...     C..  : /usr/local/maxicode/maxiaide/log/maxiaide-30-11-2020_16-45-01.3109-30-11-2020_16-45-01-report.log

---------------------------------------------------
Detailed information about changes:
---------------------------------------------------

File: /usr/local/maxicode/maxiaide/db/aide.db.gz
  SHA256   : 3PJxqv57yuv4Nd4yInYIqXhsUrza3Q2+ | +cjsF9Oj0+bUpfsQDpCktAw1U4vWS943
             5tS4kvuSruY=                     | EzSbitB1ziU=

File: /usr/local/maxicode/maxiaide/log/maxiaide-30-11-2020_16-45-01.3109-30-11-2020_16-45-01-report.log
  SHA256   : 5c/U+gYoKWu59KwyxLV2tNMchI+ZD2TL | sTonMeXFq2ugaX976GSmW7BrZC8Fq1gK
             LZfzJ+FxPXQ=                     | Dzpafppo7Vk=


---------------------------------------------------
The attributes of the (uncompressed) database(s):
---------------------------------------------------

/usr/local/maxicode/maxiaide/db/aide.db.gz
  MD5      : PoA8TZoOzNNfV6/Y5f5fMw==
  SHA1     : skLdMFIvHxxiFnm3siz7oO8+V1s=
  RMD160   : 4ahf6dVMn0IGP7a1doYFDssyobg=
  TIGER    : +gCXbCmYH5ZwfyLFduuF44MByZ2nTr8K
  SHA256   : rmuS5LcnibY0C/LDzkrzqicXIwvK50Fe
             s4fcqFxO/Bc=
  SHA512   : os6fWU69Q3BeYg2D5W0Ag3DlqBylHhfn
             ThTOjDYfgzZYaCBuE2x9NoDmeIWlVvwR
             I52cCTsUIG4A+LfWLgZSoQ==

/usr/local/maxicode/maxiaide/db/aide.db.new.gz
  MD5      : 66oMzxhiBaW1eI9bAU+bIg==
  SHA1     : EXSqd2h0fDUBlgV1+lt4N3Iy8Gc=
  RMD160   : cGu2vamCl/IveXwu2RCI2NULYOs=
  TIGER    : gNcNaxcfdSr7qqgLp7ae5J4MWne5Ghfo
  SHA256   : 380lkfL12efQTvWAFrHFc6hh2aZA7Cwm
             DBoxQdSUG54=
  SHA512   : H4LbqA2YxwcgFROM4GK68nRh8/4w6UNI
             sXW6LpmWuudqDX9n5ZhXyi+H8mxJc9Rz
             Ek8tVOE23Sxsvu3uvFu1Mg==


End timestamp: 2020-11-30 16:50:31 +0800 (run time: 2m 52s)
[maxiaide]: Warning, 4 new and 2 changed files detected
[maxiaide]: AIDE database of [/usr/local/maxicode/maxiaide/db/aide.db.new.gz] was renamed as [/usr/local/maxicode/maxiaide/db/aide.db.gz]
[maxiaide]: AIDE has finished checking

---= FILE CHECK AND UPDATE END =---

=============================================
================ SUMMARY ====================
=============================================
[maxiaide]: Running mode: [cronjob]
[maxiaide]: Scan completed successfully
[maxiaide]: Scan status: WARNING | 4 new and 2 changed files detected
[maxiaide]: Log file is located at /usr/local/maxicode/maxiaide/log//maxiaide-30-11-2020_16-47-39.6120-30-11-2020_16-47-39-report.log
=============================================
[maxiaide]: Preparing to upload log file into [onedrive] ...
[maxiaide]: Decrypting rclone config file ...
gpg: encrypted with 4096-bit RSA key, ID 082A211230147C49, created 2020-08-30
      "Arafat Ali (ArafatX Main GPG Key) <arafat@sofibox.com>"
[maxiaide]: OK, rclone config file decrypted successfully
[maxiaide]: Creating new backup directory in [onedrive] as [Earthbox/maxicode_logs/maxiaide/November/30-11-2020/] ...
[maxiaide]: OK, new report folder [Earthbox/maxicode_logs/maxiaide/November/30-11-2020/] created at [onedrive]
3.972k / 3.972 kBytes, 100%, 3.438 kBytes/s, ETA 0s
[maxiaide]: Success, report file of [/usr/local/maxicode/maxiaide/log//maxiaide-30-11-2020_16-47-39.6120-30-11-2020_16-47-39-report.log] has been successfully uploaded into [onedrive]
```

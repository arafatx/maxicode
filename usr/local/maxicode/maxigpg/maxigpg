#!/bin/bash
# Author: Arafat Ali | Email: webmaster@sofibox.com
# In gpg v2, there is no an official way how to check a valid password from gpg-agent. So this script will do the check
# First, it will ask for gpg passphrase and validate it
# If you input wrong or non-existing KEY_ID as an argument it can validate that as well
# When a valid passphrase is cached, the next time you run this script, it will not ask you to re-enter a passphrase
# It also can start gpg-agent using custom config, give remaining time when the cached passphrase will be expired
# # Check gpg cache every 1 minute (run this on cronjob):
# * * * * *       root    /usr/local/maxicode/maxigpg/maxigpg -k 2B705B8B6FA943B1 --cron-warn-only > /dev/null
# usage ./maxigpg -k <KEY_ID>   | ./maxigpg -k 2B705B8B6FA943B1
# Use case for this script: You can set this script to ask a passphrase and cache it in gpg-agent, so you can decrypt file without having to manually enter passphrase.
# This is very useful if you have scripts that need to use gpg encryption or decryption automatically. You can set expired time for the cached passphrase for security
# This is the example of real use case discussion for using gpg-agent with rclone: https://forum.rclone.org/t/rclone-mount-with-configuration-password/573
# TODO 1: send log files to onedrive cloud

# Disable shellcheck for undetermined source
# shellcheck source=/dev/null

function usage() {
  echo "${APP_SPECIFIC_NAME} Documentation"
  echo
  echo "Author: Arafat Ali | Email: arafat@sofibox.com"
  echo
  echo "Usage: "
  echo "${SCRIPT_NAME} [<options>] OPTIONAL [ -k  <KEY_ID> [ OPTIONAL <cron_warn> ]]"
  echo
  echo "options:"
  echo
  echo "  -h, --help"
  echo "      This help text."
  echo
  echo "  -v, --version"
  echo "      Show version information"
  echo
  echo "  -i, --init"
  echo "      Remove all configuration, passphrases, stop gpg-agent process"
  echo "      and recreate new configuration files (init default setting)"
  echo
  echo "  -cp, --clearpass"
  echo "      Clear all gpg-agent passphrases"
  echo
  echo "  -x, --stop"
  echo "      Stop gpg-agent process. This will also clear the passphrase"
  echo
  echo "  -s, --status OPTIONAL <KEY_ID>"
  echo "      eg: ${SCRIPT_NAME} -s OPTIONAL <KEY_ID>"
  echo "      Show gpg-agent process and cache status for a given key ID. KEY_ID is an optional argument"
  echo "      If no KEY_ID argument is given, it will then find the current valid passphrase with its cached KEY_ID"
  echo "      If KEY_ID argument is empty and not in cache file, program will halt for correct KEY_ID."
  echo
  echo "  -rconf, --removeconf"
  echo "      This will remove gpg-agent configuration files. The new configuration will be created"
  echo "      automatically when the script is running again"
  echo
  echo "  -k, --set, --setpass, --setpassword, --cache, --key <KEY_ID> OPTIONAL<cron_warn>"
  echo "      eg: ${SCRIPT_NAME} -k <KEY_ID> <cron_warn>"
  echo "      eg: ${SCRIPT_NAME} -k 2B705B8B6FA943B1 --cron-warn-only"

}

# This function save configuration files with variable assignment pattern for GPG_MAIN_CONF
function set_config() {
  CONF_FILE="$3"
  sed -i "s/^\($1\s*=\s*\).*\$/\1$2/" "${CONF_FILE}"
}

# This function converts seconds into days, hours, minutes and seconds
function show_time() {
  local num=$1
  local min=0
  local hour=0
  local day=0
  if ((num > 59)); then
    ((sec = num % 60))
    ((num = num / 60))
    if ((num > 59)); then
      ((min = num % 60))
      ((num = num / 60))
      if ((num > 23)); then
        ((hour = num % 24))
        ((day = num / 24))
      else
        ((hour = num))
      fi
    else
      ((min = num))
    fi
  else
    ((sec = num))
  fi
  echo "$day"d "$hour"h "$min"m "$sec"s
}

function update_pid_count() {
  PIDOF_COUNT=$(pidof -x gpg-agent | wc -w)
}

# This standalone function clears all gpg passphrases
# Usage: maxigpg --clearpass
function gpg_clearpass() {
  local status
  echo "[${SCRIPT_NAME}]: Forgetting all passphrases from gpg-agent ..." | tee -a "${REPORT_FILE}"
  # This does not generate new PID, So this is the way to forget passphrase
  # Only do this when gpg-agent is running because the gpg-connect-agent command will automatically spawn a gpg-agent process (the gpg-agent behavioural problem)
  if [ "${PIDOF_COUNT}" -gt 0 ]; then
    status=$(echo RELOADAGENT | gpg-connect-agent)
    echo "[${SCRIPT_NAME}]: ${status}" | tee -a "${REPORT_FILE}"
  else
    echo "[${SCRIPT_NAME}]: Warning, no passphrase to clear because gpg-agent is not running!" | tee -a "${REPORT_FILE}"
  fi
}

# This standalone function remove gpg-agent config files
# Usage: maxigpg --removeconf
function gpg_removeconf() {
  echo "[${SCRIPT_NAME}]: Removing config files ... "
  rm -f "${GPG_AGENT_CONF}"
  rm -f "${GPG_MAIN_CONF}"
  echo "[${SCRIPT_NAME}]: Ok, config files have been removed"
}

# This standalone function stop gpg-agent process
# Usage: maxigpg --stop
function gpg_stop() {
  local ret status
  echo "[${SCRIPT_NAME}]: Stopping gpg-agent and clear all passphrases cached from gpg-agent ... " | tee -a "${REPORT_FILE}"
  gpgconf --kill gpg-agent
  ret=$?
  if [ ${ret} = 0 ]; then
    echo "[${SCRIPT_NAME}]: OK, gpg-agent has been stopped!" | tee -a "${REPORT_FILE}"
  else
    echo "[${SCRIPT_NAME}]: Error, something is wrong when stopping gpg-agent [return code: ${ret}]" | tee -a "${REPORT_FILE}"
  fi
}

function init_config() {
  # 2) Check if gpg-agent config file exist. If doesn't exist,
  # then write some default values (run ./maxigpg --init after making changes to this config)
  if [ ! -e "${GPG_AGENT_CONF}" ]; then
    echo "[${SCRIPT_NAME}]: Writing new GPG config file at ${GPG_AGENT_CONF} ..." | tee -a "${REPORT_FILE}"
    echo "-------" | tee -a "${REPORT_FILE}"
    touch "${GPG_AGENT_CONF}"
    # Just put the default value
    echo "allow-preset-passphrase" | tee --append "${GPG_AGENT_CONF}" | tee -a "${REPORT_FILE}"
    echo "default-cache-ttl 34560000" | tee --append "${GPG_AGENT_CONF}" | tee -a "${REPORT_FILE}"
    echo "max-cache-ttl 34560000" | tee --append "${GPG_AGENT_CONF}" | tee -a "${REPORT_FILE}"
    echo "-------" | tee -a "${REPORT_FILE}"
    echo "" | tee -a "${REPORT_FILE}"
  fi

  # 3) Check if gpg-agent cached config file exist. If it doesn't exist,
  # then write some default values (run ./maxigpg init after making changes to this config)
  if [ ! -e "${GPG_MAIN_CONF}" ]; then
    echo "[${SCRIPT_NAME}]: Writing new config file at ${GPG_MAIN_CONF} ..." | tee -a "${REPORT_FILE}"
    echo "-------" | tee -a "${REPORT_FILE}"
    touch "${GPG_MAIN_CONF}"
    # Set default values
    echo "date_cached=$(date +%s)" | tee --append "${GPG_MAIN_CONF}" | tee -a "${REPORT_FILE}"
    echo "process_id='-1'" | tee --append "${GPG_MAIN_CONF}" | tee -a "${REPORT_FILE}"
    echo "cached_key_ids='-1'" | tee --append "${GPG_MAIN_CONF}" | tee -a "${REPORT_FILE}"
    expired_in_second=$(grep -oP 'max-cache-ttl\s*\K\d+' "${GPG_AGENT_CONF}")
    echo "expired_in=${expired_in_second}" | tee --append "${GPG_MAIN_CONF}" | tee -a "${REPORT_FILE}"
    echo "-------" | tee -a "${REPORT_FILE}"
    echo "" | tee -a "${REPORT_FILE}"
    # Source the custom setting, we need this initial values
    source "${GPG_MAIN_CONF}"
  fi
}

# This standalone function remove gpg-agent config files and stop its process
# Usage: maxigpg --init
function gpg_init() {
  gpg_removeconf
  gpg_stop
  init_config

}

# This function output PID information for one or more gpg-agents

function pid_output() {
  # This functions will be called many times,
  # so get the new setting for each call to detect new process and compare with the setting
  echo "" | tee -a "${REPORT_FILE}"
  source "${GPG_MAIN_CONF}"
  local p_count pidof_count
  p_count=0
  # PID is global variable (dont make it local)
  for PID in $(pidof gpg-agent | tr ' ' '\n'); do
    ((p_count += 1))
    echo "---------------------------" | tee -a "${REPORT_FILE}"
    echo "--- (${p_count}) PID: ${PID} ---" | tee -a "${REPORT_FILE}"
    echo "Process ID details:" | tee -a "${REPORT_FILE}"
    ps -p "${PID}" -o pid,vsz=MEMORY -o user,group=GROUP -o comm,args=ARGS | tee -a "${REPORT_FILE}"
    echo "Process TREE details:" | tee -a "${REPORT_FILE}"
    pstree -sang "${PID}" | tee -a "${REPORT_FILE}"
    echo "" | tee -a "${REPORT_FILE}"
    # shellcheck disable=SC2154
    if [ "${PID}" -eq "${process_id}" ]; then
      # Show a peace message
      echo "OK, current gpg-agent PID (${p_count})[${PID}] is matched with the cached PID [${process_id}]" | tee -a "${REPORT_FILE}"
    else
      # Show warning message
      echo "Warning, current gpg-agent PID (${p_count})[${PID}] does not match with the cached gpg-agent PID [${process_id}]" | tee -a "${REPORT_FILE}"
    fi
    echo "---------------------------" | tee -a "${REPORT_FILE}"
  done
  pidof_count=$(pidof -x gpg-agent | wc -w)
  if [ "${pidof_count}" -gt 1 ]; then
    echo "[${SCRIPT_NAME}]: Warning, there are ${pidof_count} gpg-agent processes that are currently running: [$(pidof gpg-agent)]" | tee -a "${REPORT_FILE}"
    echo "[${SCRIPT_NAME}]: This is a known mystery-bug and it happens only when a 'sudo' command is being executed from cronjob: https://dev.gnupg.org/T5076" | tee -a "${REPORT_FILE}"
    echo "[${SCRIPT_NAME}]: 1) The bad thing about this is, the passphrase that have been cached manually from all running gpg-agents before will become invalid until the other new random process(es) are terminated or" | tee -a "${REPORT_FILE}"
    echo "[${SCRIPT_NAME}]: 2) You can just re-enter the same passphrase again to cache in the new gpg-agent random process(es) and you will have valid passphrase again or" | tee -a "${REPORT_FILE}"
    echo "[${SCRIPT_NAME}]: 3) You can wait until the new random gpg-agent processes terminate itself (normally after a sudo command has finished running from cronjob)" | tee -a "${REPORT_FILE}"
    exit 1
  fi
}

# This function show gpg-agent run status
function gpg_runstatus() {

  if [ ! -e "${GPG_MAIN_CONF}" ]; then
    echo "[${SCRIPT_NAME}]: Warning, [${GPG_MAIN_CONF}] file config is missing. Please run maxigpg --init first"
    exit 1
  fi
  if [ ! -e "${GPG_AGENT_CONF}" ]; then
    echo "[${SCRIPT_NAME}]: Warning, [${GPG_AGENT_CONF}] file config is missing. Please run maxigpg --init first"
    exit 1
  fi

  # shellcheck disable=SC2154
  local KEY_ID
  KEY_ID="$1"

  if [ "${PIDOF_COUNT}" -gt 0 ]; then
    if [ -z "${KEY_ID}" ]; then
      source "${GPG_MAIN_CONF}"
      # shellcheck disable=SC2154
      KEY_ID="${cached_key_ids}"
      if [ "${KEY_ID}" == "-1" ]; then
        echo "[${SCRIPT_NAME}]: Error, missing <KEY_ID> argument and from cached. Please, specify key manually. e.g: ${SCRIPT_NAME} ${opt} 2B705B8B6FA943B1"
        exit 1
      else
        echo "[${SCRIPT_NAME}]: Found KEY_ID [${KEY_ID}] in cache. Checking ${KEY_ID} status ..."
      fi
    fi
    echo "[${SCRIPT_NAME}]: OK, gpg-agent daemon is already running with the following process ID(s):"
    VALID_STATUS=$({ echo "1234" | gpg -q --pinentry-mode=loopback --status-fd 1 --sign --local-user "${KEY_ID}" --passphrase-fd 0 >/dev/null; } 2>&1)
    RET_VAL=$?
    if [ "${RET_VAL}" = 0 ]; then
      pid_output
      echo "[${SCRIPT_NAME}]: OK, passphrase is valid for ${KEY_ID}."
    else
      pid_output
      echo "[${SCRIPT_NAME}]: Warning, no valid cache passphrase found for ${KEY_ID}."
    fi
    echo "[${SCRIPT_NAME}]: Validation return status: "
    echo "*------*"
    if [ -n "${VALID_STATUS}" ]; then
      echo "${VALID_STATUS}"
    else
      echo "OK"
    fi
    echo "*------*"
  else
    echo "[${SCRIPT_NAME}]: Warning, no gpg-agent is running."
  fi
}

# This function show valid information about cached passphrase
function gpg_show_valid_info() {
  # Refresh new setting (must have this)
  source "${GPG_MAIN_CONF}"
  expired_in_second=$(grep -oP 'max-cache-ttl\s*\K\d+' "${GPG_AGENT_CONF}")
  expired_date=$(date -d "$(date -d @"${date_cached}") + ${expired_in_second} seconds")
  date_cached=$(date -d @"${date_cached}")
  second_left="$(($(date -d "${expired_date}" "+%s") - $(date +%s)))"
  echo "[${SCRIPT_NAME}]: The passphrase is valid with the following information:" | tee -a "${REPORT_FILE}"
  echo "------------------------------" | tee -a "${REPORT_FILE}"
  echo "Valid from: [${date_cached}]" | tee -a "${REPORT_FILE}"
  echo "Expired on: [${expired_date}]" | tee -a "${REPORT_FILE}"
  echo "Expired in: [$(show_time ${second_left})]" | tee -a "${REPORT_FILE}"
  echo "------------------------------" | tee -a "${REPORT_FILE}"
}

# Update 2: Cache a RAW passphrase into gpg-agent, if success, then validate the cached passphrase (gpg_validatepass)
# If RAW passphrase is not cached then print error
# Must run gpg-agent first before cache
function set_gpg_cachepass() {
  source "${GPG_MAIN_CONF}"
  read -r -s -p "[${SCRIPT_NAME}]: Enter a passphrase to cache into a running gpg-agent [$(pidof gpg-agent)]: " PASSPHRASE
  echo
  ${GPG_PRESET_PASS} -c "${KEY_GRIP}" <<<"${PASSPHRASE}"
  #gpg --export-secret-keys -a "$KEY_ID" > /dev/null
  RET_VAL=$?
  if [ $RET_VAL = 0 ]; then
    set_config date_cached "$(date +%s)" "${GPG_MAIN_CONF}"
    expired_in_second=$(grep -oP 'max-cache-ttl\s*\K\d+' "${GPG_AGENT_CONF}")
    set_config expired_in "${expired_in_second}" "${GPG_MAIN_CONF}"
    gpg_validatepass
  else
    WARN_STATUS="WARNING"
    echo "[${SCRIPT_NAME}]: Unsuccessful error occurred with return code: [$RET_VAL]" | tee -a "$REPORT_FILE"
  fi
}

function gpg_validatepass() {
  echo "[${SCRIPT_NAME}]: Now, validating the cached passphrase input ..." | tee -a "${REPORT_FILE}"
  # It is important to set RAW gpg passphrase first before validate
  echo "-------" | tee -a "${REPORT_FILE}"
  echo "1234" | gpg -q --pinentry-mode=loopback --status-fd 1 --sign --local-user "${KEY_ID}" --passphrase-fd 0 >/dev/null
  RET_VAL=$?
  if [ "${RET_VAL}" = 0 ]; then
    echo "[${SCRIPT_NAME}]: OK, a passphrase is valid in gpg-agent [$(pidof gpg-agent)]" | tee -a "${REPORT_FILE}"
    # Need this as a final status
    WARN_STATUS="OK"
    update_pid_count
    #source "${GPG_MAIN_CONF}"
    if [ "${PIDOF_COUNT}" -eq 1 ]; then
      set_config process_id "$(pidof gpg-agent)" "${GPG_MAIN_CONF}"
      # shellcheck disable=SC2154
      set_config cached_key_ids "${KEY_ID}" "${GPG_MAIN_CONF}"
    else
      echo "[${SCRIPT_NAME}]: Warning, skipped caching process_id [$(pidof gpg-agent)] because there are more than 1 gpg-agent process is running!" | tee -a "${REPORT_FILE}"
    fi
    gpg_show_valid_info
    # Send email for the first time when it is valid.
    if [ "${CRON_WARN}" == "--cron-warn-only" ]; then
      : # Do nothing
    else
      echo "[${SCRIPT_NAME}]: Status is [${WARN_STATUS}]" | tee -a "${REPORT_FILE}"
      $MAIL_BIN -s "[${SCRIPT_NAME} | ${WARN_STATUS}]: OK, gpg-agent now has a valid cached passphrase" "${ADMIN_EMAIL}" <"${REPORT_FILE}"
    fi
  else
    echo "-------" | tee -a "${REPORT_FILE}"
    echo "" | tee -a "${REPORT_FILE}"
    WARN_STATUS="WARNING"
    echo "[${SCRIPT_NAME}]: Error, invalid passphrase is cached in gpg-agent. Validation return code: [$RET_VAL]" | tee -a "${REPORT_FILE}"
    # clear passphrase because why do we need to store invalid passphrase ?
    gpg_clearpass
  fi
}

# Email to send report
ADMIN_EMAIL="webmaster@sofibox.com"
APP_SPECIFIC_NAME="MaxiGPG"

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
SCRIPT_NAME=$(basename -- "$0")
GPG_PRESET_PASS="/usr/libexec/gpg-preset-passphrase"
BOX_HOSTNAME=$(hostname)
DATE_BIN=$(command -v date)
MAIL_BIN=$(command -v mail)

LOG_PATH="${SCRIPT_PATH}/log"
CONFIG_PATH="${SCRIPT_PATH}/conf"
SECURE_PATH="${SCRIPT_PATH}/secure"
GPG_AGENT_CONF="${CONFIG_PATH}/gpg-agent.conf"
#GPG_AGENT_ENV="$CONFIG_PATH/gpg-agent.env"
GPG_MAIN_CONF="${CONFIG_PATH}/gpg-main.conf"

# Create log and config directory
mkdir -p "${LOG_PATH}"
mkdir -p "${CONFIG_PATH}"
mkdir -p "${SECURE_PATH}"
# Generate random string based on date
RANDSTR="$(${DATE_BIN} '+%d-%m-%Y_%H-%M-%S').${RANDOM}"
DATE_TIME_NOW="$(date '+%d-%m-%Y_%H-%M-%S')" #31-03-2020--11-56-16
REPORT_FILE="${LOG_PATH}/${SCRIPT_NAME}-${RANDSTR}-${DATE_TIME_NOW}-report.log"
GPG_TTY=$(tty)
export GPG_TTY
WARN_STATUS="OK"
#KEY_ID=2B705B8B6FA943B1

RET_VAL=1

# Clear log file for 1 day
find "${LOG_PATH}" -name "*.log" -mtime +1 -exec rm {} \;
# initialize an empty log file
cat /dev/null >"${REPORT_FILE}"

# INITIALIZE PROCESS COUNT - next just run update_pid_count
PIDOF_COUNT=$(pidof -x gpg-agent | wc -w)

ARGNUM="$#"
# Handle option arguments
if [ $ARGNUM -eq 0 ]; then
  echo "[${SCRIPT_NAME}]: Error, no argument is supplied. Use --help to see the valid options"
fi

while [ "$#" -gt 0 ]; do
  case "$1" in
  # Display help and usage
  -h | --help)
    usage
    exit 0
    ;;
  -v | --version) # Display Program version
    echo "v0.3 - ${APP_SPECIFIC_NAME} by MaXi32 (Arafat Ali - sofibox.com)"
    exit 0
    break
    ;;
  -i | --init)
    gpg_init
    rm -f "${REPORT_FILE}"
    exit 0
    break
    ;;
  -cp | --clearpass)
    gpg_clearpass
    rm -f "${REPORT_FILE}"
    exit 0
    break
    ;;
  -x | --stop)
    gpg_stop
    rm -f "${REPORT_FILE}"
    exit 0
    break
    ;;
  -rconf | --removeconf)
    gpg_removeconf
    rm -f "${REPORT_FILE}"
    exit 0
    break
    ;;
  -getpath | --getpath)
    opt="$1"
    path="$2"
    if [ "${path}" == "config" ]; then
      echo "${CONFIG_PATH}"
    fi
    if [ "${path}" == "secure" ]; then
      echo "${SECURE_PATH}"
    fi
    exit 0
    break
    ;;
  -set | -k | --set | --setpass | --setpassword | --cache | --key)
    opt="$1"
    KEY_ID="$2"
    CRON_WARN="$3"
    if [ -z "${KEY_ID}" ]; then
      echo "[${SCRIPT_NAME}]: Error, missing <KEY_ID>. e.g: ${SCRIPT_NAME} ${opt} 2B705B8B6FA943B1"
      exit 1
    fi
    # 1) Check gpg-agent is running or not. If it is not running, show warning and run it
    if [ "${PIDOF_COUNT}" -gt 0 ]; then
      # If it is running, then just display the PID information:
      echo "[${SCRIPT_NAME}]: OK, gpg-agent daemon is already running with the following process ID(s):" | tee -a "${REPORT_FILE}"
      pid_output
    else
      WARN_STATUS="WARNING"
      echo "[${SCRIPT_NAME}]: Warning, no gpg-agent daemon is running!" | tee -a "${REPORT_FILE}"
      # This is use to debug warning message in cronjob
      if [ "${CRON_WARN}" == "--cron-warn-only" ]; then
        echo "[${SCRIPT_NAME}]: Warning, gpg-agent is not running!" | tee -a "${REPORT_FILE}"
        echo "[${SCRIPT_NAME}]: To cache a passphrase in gpg-agent, run the following script manually in the terminal: [./${SCRIPT_NAME} ${KEY_ID}]" | tee -a "${REPORT_FILE}"
        echo "[${SCRIPT_NAME}]: Script is now terminated!" | tee -a "${REPORT_FILE}"
        $MAIL_BIN -s "[${SCRIPT_NAME}]: Warning, gpg-agent is not running!" ${ADMIN_EMAIL} <"${REPORT_FILE}"
        # Don't let the cronjob run the daemon so exit this script
        exit 1
      fi

      # 2, 3
      init_config

      echo "[${SCRIPT_NAME}]: Starting gpg-agent daemon with config from [${GPG_AGENT_CONF}] ..." | tee -a "${REPORT_FILE}"
      gpg-agent --options "${GPG_AGENT_CONF}" --daemon
      RET_VAL=$?
      # Need to update the new PIDOF_COUNT after running daemon
      update_pid_count
      if [ "${RET_VAL}" -eq 0 ]; then
        pid_output
        echo "[${SCRIPT_NAME}]: OK, gpg-agent daemon is now running with the following process id(s): [$(pidof gpg-agent)]" | tee -a "${REPORT_FILE}"
        # Write the PID in the config file
        set_config process_id "$(pidof gpg-agent)" "${GPG_MAIN_CONF}"
        # Refresh new setting
        source "${GPG_MAIN_CONF}"
        if [ "${PID}" -eq "${process_id}" ]; then
          echo "[${SCRIPT_NAME}]: OK, current gpg-agent PID [${PID}] is now matched with the cached gpg-agent PID [${process_id}]" | tee -a "${REPORT_FILE}"
        fi
      else
        echo "[${SCRIPT_NAME}]: Error, something is wrong when starting gpg-agent daemon [return code: ${RET_VAL}]" | tee -a "${REPORT_FILE}"
      fi
    fi

    # 4) Check if KEY_ID given is correct and not empty
    # gpg-agent need to run first from the above function before executing this, else it won't work
    # Get the KEY_GRIP value to be used in function above
    update_pid_count
    if [ "$PIDOF_COUNT" -eq 1 ]; then
      # KEY_GRIP=$(gpg --with-keygrip --list-secret-keys "$KEY_ID" | grep -Pom1 '^ *Keygrip += +\K.*')
      KEY_GRIP=$({ gpg --with-keygrip --list-secret-keys "$KEY_ID" | grep -Pom1 '^ *Keygrip += +\K.*'; } 2>&1)
      RET_VAL=$?
      if [[ $RET_VAL -eq 0 ]]; then
        KEY_GRIP="${KEY_GRIP}"
      else
        echo "[${SCRIPT_NAME}]: Warning, Found error: [ ${KEY_GRIP} ]" | tee -a "${REPORT_FILE}"
        echo "[${SCRIPT_NAME}]: usage: ./${SCRIPT_NAME} -k <KEY_ID>" | tee -a "${REPORT_FILE}"
        exit 1
      fi

    fi

    # 5) Check if a RAW passphrase is cached (FLAG=1). RAW passphrase can be either valid or not valid passphrase.
    RAW_PASS_FLAG=$(echo "KEYINFO --no-ask $KEY_GRIP Err Pmt Des" | gpg-connect-agent | awk '{ print $7 }')
    if [ "${RAW_PASS_FLAG}" == "1" ]; then
      # If a RAW passphrase if found in the cache (FLAG=1), then print ok
      echo "[${SCRIPT_NAME}]: OK, a passphrase is already cached in gpg-agent [${process_id}] for key ID of [${KEY_ID}]" | tee -a "${REPORT_FILE}"
      # Then validate the RAW passphrase with this function
      gpg_validatepass
    else
      # If a RAW passphrase is not found, in the cache, then print warning
      WARN_STATUS="WARNING"
      echo "[${SCRIPT_NAME}]: Warning, no cached passphrase is detected in gpg-agent for key ID of [$KEY_ID]" | tee -a "${REPORT_FILE}"
      # Debug in cronjob message
      if [ "${CRON_WARN}" == "--cron-warn-only" ]; then
        echo "[${SCRIPT_NAME}]: To cache a passphrase in gpg-agent, run the following script manually in the terminal: [./${SCRIPT_NAME} $KEY_ID]" | tee -a "${REPORT_FILE}"
        echo "[${SCRIPT_NAME}]: Script is now terminated!" | tee -a "${REPORT_FILE}"
        echo "[${SCRIPT_NAME}]: Status is [${WARN_STATUS}]" | tee -a "${REPORT_FILE}"
        $MAIL_BIN -s "[${SCRIPT_NAME} | ${WARN_STATUS}]: Warning, gpg-agent has no cached passphrase!" ${ADMIN_EMAIL} <"${REPORT_FILE}"
        exit 1
      fi
      # After that call this function to set a RAW passphrase
      set_gpg_cachepass
    fi

    # 6) Print overall status

    echo "[${SCRIPT_NAME}]: Overall status is [$WARN_STATUS]" | tee -a "${REPORT_FILE}"

    if [ "${WARN_STATUS}" == "WARNING" ]; then
      $MAIL_BIN -s "[${SCRIPT_NAME} | ${WARN_STATUS}]: GPG Passphrase Cache Report @ ${BOX_HOSTNAME}" ${ADMIN_EMAIL} <"${REPORT_FILE}"
    fi

    exit 1
    break
    ;;
  -s | --status)
    opt="$1"
    KEY_ID="$2"
    #if [ -z "${KEY_ID}" ]; then
    #  echo "[${SCRIPT_NAME}]: Error, missing <KEY_ID>. e.g: ${SCRIPT_NAME} ${opt} 2B705B8B6FA943B1"
    #  exit 1
    #fi
    gpg_runstatus "${KEY_ID}"
    break
    ;;
  --)
    break
    ;;
  -*)
    opt="${opt}"
    echo "Invalid option '${opt}'. Use --help to see the valid options"
    exit 1
    ;;
  # an option argument, continue
  *) ;;
  esac
  shift
done
